CREATE TABLE NTF_FILES
   (	ID NUMBER(*,0) NOT NULL ENABLE, 
	FILE_NAME VARCHAR2(100 BYTE) NOT NULL ENABLE, 
	FILE_PATH VARCHAR2(200 BYTE) NOT NULL ENABLE, 
	FILE_TYPE NUMBER(*,0) NOT NULL ENABLE, 
	FILE_LINES_NUMBER NUMBER(*,0) NOT NULL ENABLE, 
	PAYLOAD VARCHAR2(250 BYTE), 
	INSERT_DATE TIMESTAMP (6) NOT NULL ENABLE, 
	LAST_UPDATE_DATE TIMESTAMP (6) NOT NULL ENABLE, 
	STATUS NUMBER,
	 CONSTRAINT TABLE1_PK PRIMARY KEY (ID)
   ) ;
   
  
  
   

  CREATE TABLE NTF_FILE_LINES
   (	ID NUMBER(*,0) NOT NULL ENABLE, 
	FILE_ID NUMBER(*,0) NOT NULL ENABLE, 
	LINE_NUMBER NUMBER(*,0) NOT NULL ENABLE, 
	MSISDN VARCHAR2(20 BYTE) NOT NULL ENABLE, 
	INSTALLATION_ID VARCHAR2(500 BYTE) NOT NULL ENABLE, 
	OS_TYPE NUMBER(*,0), 
	PAYLOAD VARCHAR2(250 BYTE), 
	STATUS NUMBER(*,0) NOT NULL ENABLE, 
	INSERTED_DATE TIMESTAMP (6) NOT NULL ENABLE, 
	LAST_UPDATE_DATE TIMESTAMP (6) NOT NULL ENABLE, 
	RESPONSE_MESSAGE VARCHAR2(1000 BYTE), 
	IS_INSTALLED CHAR(1 BYTE),
	 CONSTRAINT NTF_FILE_LINES_PK PRIMARY KEY (ID)
   );
   
    CREATE INDEX NTF_FILE_LINES_FILE_ID ON NTF_FILE_LINES ("FILE_ID");
  
	CREATE INDEX NTF_FILE_LINES_INSTALLATION_ID ON NTF_FILE_LINES ("INSTALLATION_ID");
  
	CREATE INDEX NTF_FILES_FILE_PATH ON NTF_FILES ("FILE_PATH");  
	
    CREATE SEQUENCE  NTF_FILE_LINES_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 5 NOCACHE  ORDER  NOCYCLE ;
	
	CREATE SEQUENCE  NTF_FILES_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  ORDER  NOCYCLE ;
