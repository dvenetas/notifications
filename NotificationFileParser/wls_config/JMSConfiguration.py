loadProperties('jms.properties')
print(url)
connect(username,password, url)

edit()

startEdit()

filestoreName = jmsResourcesName+'JMSFilestore'
filestoreLocation = '/FileStores/'+filestoreName
filestoreDirectory = 'jmsFilestores/'+filestoreName
jmsServerName = jmsResourcesName+'JMSServer'
jmsModuleName = jmsResourcesName+'JMSModule'
subDeploymentName = jmsResourcesName+'SubDeployment'



print 'Creating File Store'
cd('/')
cmo.createFileStore(filestoreName)
cd(filestoreLocation)
cmo.setDirectory(filestoreDirectory)
set('Targets',jarray.array([ObjectName('com.bea:Name='+targetServerName+',Type=Server')], ObjectName))

validate()
save()
activate()

edit()
startEdit()

print 'Creating JMS Server'
cd('/')
print 'Creating JMS Server.'
cmo.createJMSServer(jmsServerName)
cd('/JMSServers/'+jmsServerName)
cmo.setPersistentStore(getMBean(filestoreLocation))
cmo.setTemporaryTemplateResource(None)
cmo.setTemporaryTemplateName(None)
cmo.addTarget(getMBean('/Servers/'+targetServerName))

validate()
save()
activate()

edit()
startEdit()

print 'Creating JMS Module'
cd('/')
cmo.createJMSSystemResource(jmsModuleName)
cd('/JMSSystemResources/'+jmsModuleName)
cmo.addTarget(getMBean('/Servers/'+targetServerName))
cmo.createSubDeployment(subDeploymentName)

jmsModuleResources = '/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName
jmsConnectionFactoryName = jmsResourcesName+'ConnectionFactory'
fullyQualifiedConnectionFactory = jmsModuleResources+'/ConnectionFactories/'+jmsConnectionFactoryName

validate()
save()
activate()

edit()
startEdit()

print 'Creating Connection Factory'
cd('/')
cd(jmsModuleResources)
cmo.createConnectionFactory(jmsConnectionFactoryName)
cd(fullyQualifiedConnectionFactory)
cmo.setJNDIName('jms/cf/'+jmsConnectionFactoryName)
cmo.setDefaultTargetingEnabled(true)
#set('SubDeploymentName',subDeploymentName)
cd(fullyQualifiedConnectionFactory+'/SecurityParams/'+jmsConnectionFactoryName)
cmo.setAttachJMSXUserId(false)
cd(fullyQualifiedConnectionFactory+'/ClientParams/'+jmsConnectionFactoryName)
cmo.setClientIdPolicy('Restricted')
cmo.setSubscriptionSharingPolicy('Exclusive')
cmo.setMessagesMaximum(10)
cd(fullyQualifiedConnectionFactory+'/TransactionParams/'+jmsConnectionFactoryName)
cmo.setTransactionTimeout(3600)
cmo.setXAConnectionFactoryEnabled(true)


jmsErrQueueName = jmsResourcesName+'ErrorJMSQueue'
jmsQueueName = jmsResourcesName+'JMSQueue'
jmsErrQueueName1 = 'NotificationSendingErrorJMSQueue'
jmsQueueName1 = 'NotificationSendingJMSQueue'
jmsErrQueueName2 = 'NotificationFilesReportErrorJMSQueue'
jmsQueueName2 = 'NotificationFilesReportJMSQueue'
jmsErrQueueName3 = 'NotificationUpdateLineStatusErrorJMSQueue'
jmsQueueName3 = 'NotificationUpdateLineStatusJMSQueue'

validate()
save()
activate()

edit()
startEdit()
print 'Creating Error Queues'
cd('/')
cd(jmsModuleResources)
cmo.createQueue(jmsErrQueueName)
cd(jmsModuleResources+'/Queues/'+jmsErrQueueName)
set('JNDIName','jms/queue/'+jmsErrQueueName)
set('SubDeploymentName',subDeploymentName)
cd('/JMSSystemResources/'+jmsModuleName+'/SubDeployments/'+subDeploymentName)
cmo.addTarget(getMBean('/JMSServers/'+jmsServerName))

cd('/')
cd(jmsModuleResources)
cmo.createQueue(jmsErrQueueName1)
cd(jmsModuleResources+'/Queues/'+jmsErrQueueName1)
set('JNDIName','jms/queue/'+jmsErrQueueName1)
set('SubDeploymentName',subDeploymentName)
cd('/JMSSystemResources/'+jmsModuleName+'/SubDeployments/'+subDeploymentName)
cmo.addTarget(getMBean('/JMSServers/'+jmsServerName))

cd('/')
cd(jmsModuleResources)
cmo.createQueue(jmsErrQueueName2)
cd(jmsModuleResources+'/Queues/'+jmsErrQueueName2)
set('JNDIName','jms/queue/'+jmsErrQueueName2)
set('SubDeploymentName',subDeploymentName)
cd('/JMSSystemResources/'+jmsModuleName+'/SubDeployments/'+subDeploymentName)
cmo.addTarget(getMBean('/JMSServers/'+jmsServerName))

cd('/')
cd(jmsModuleResources)
cmo.createQueue(jmsErrQueueName3)
cd(jmsModuleResources+'/Queues/'+jmsErrQueueName3)
set('JNDIName','jms/queue/'+jmsErrQueueName3)
set('SubDeploymentName',subDeploymentName)
cd('/JMSSystemResources/'+jmsModuleName+'/SubDeployments/'+subDeploymentName)
cmo.addTarget(getMBean('/JMSServers/'+jmsServerName))

validate()
save()
activate()
edit()
startEdit()

print 'Creating Queues'
cd('/')
cd(jmsModuleResources)
cmo.createQueue(jmsQueueName)
cd(jmsModuleResources+'/Queues/'+jmsQueueName)
set('JNDIName','jms/queue/'+jmsQueueName)
set('SubDeploymentName',subDeploymentName)
cd('/JMSSystemResources/'+jmsModuleName+'/SubDeployments/'+subDeploymentName)
cmo.addTarget(getMBean('/JMSServers/'+jmsServerName))
cd(jmsModuleResources+'/Queues/'+jmsQueueName+'/DeliveryFailureParams/'+jmsQueueName)
cmo.setRedeliveryLimit(10)
cmo.setErrorDestination(getMBean(jmsModuleResources+'/Queues/'+jmsErrQueueName))
cd(jmsModuleResources+'/Queues/'+jmsQueueName+'/DeliveryParamsOverrides/'+jmsQueueName)
cmo.setRedeliveryDelay(30000)

cd('/')
cd(jmsModuleResources)
cmo.createQueue(jmsQueueName1)
cd(jmsModuleResources+'/Queues/'+jmsQueueName1)
set('JNDIName','jms/queue/'+jmsQueueName1)
set('SubDeploymentName',subDeploymentName)
cd('/JMSSystemResources/'+jmsModuleName+'/SubDeployments/'+subDeploymentName)
cmo.addTarget(getMBean('/JMSServers/'+jmsServerName))
cd(jmsModuleResources+'/Queues/'+jmsQueueName1+'/DeliveryFailureParams/'+jmsQueueName1)
cmo.setErrorDestination(getMBean(jmsModuleResources+'/Queues/'+jmsErrQueueName1))
cmo.setRedeliveryLimit(10)
cd(jmsModuleResources+'/Queues/'+jmsQueueName1+'/DeliveryParamsOverrides/'+jmsQueueName1)
cmo.setRedeliveryDelay(30000)

cd('/')
cd(jmsModuleResources)
cmo.createQueue(jmsQueueName2)
cd(jmsModuleResources+'/Queues/'+jmsQueueName2)
set('JNDIName','jms/queue/'+jmsQueueName2)
set('SubDeploymentName',subDeploymentName)
cd('/JMSSystemResources/'+jmsModuleName+'/SubDeployments/'+subDeploymentName)
cmo.addTarget(getMBean('/JMSServers/'+jmsServerName))
cd(jmsModuleResources+'/Queues/'+jmsQueueName2+'/DeliveryFailureParams/'+jmsQueueName2)
cmo.setErrorDestination(getMBean(jmsModuleResources+'/Queues/'+jmsErrQueueName2))
cmo.setRedeliveryLimit(10)
cd(jmsModuleResources+'/Queues/'+jmsQueueName2+'/DeliveryParamsOverrides/'+jmsQueueName2)
cmo.setRedeliveryDelay(30000)

cd('/')
cd(jmsModuleResources)
cmo.createQueue(jmsQueueName3)
cd(jmsModuleResources+'/Queues/'+jmsQueueName3)
set('JNDIName','jms/queue/'+jmsQueueName3)
set('SubDeploymentName',subDeploymentName)
cd('/JMSSystemResources/'+jmsModuleName+'/SubDeployments/'+subDeploymentName)
cmo.addTarget(getMBean('/JMSServers/'+jmsServerName))
cd(jmsModuleResources+'/Queues/'+jmsQueueName3+'/DeliveryFailureParams/'+jmsQueueName3)
cmo.setErrorDestination(getMBean(jmsModuleResources+'/Queues/'+jmsErrQueueName3))
cmo.setRedeliveryLimit(10)
cd(jmsModuleResources+'/Queues/'+jmsQueueName3+'/DeliveryParamsOverrides/'+jmsQueueName3)
cmo.setRedeliveryDelay(30000)

validate()
print 'JMS Resources are Successfully Created'
save()
activate()