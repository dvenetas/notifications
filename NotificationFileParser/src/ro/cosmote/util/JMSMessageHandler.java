package ro.cosmote.util;

import java.util.Hashtable;
import java.util.Properties;

import javax.naming.*;
import javax.jms.*;

import ro.cosmote.exception.WrongServerConfigurationException;

public class JMSMessageHandler {
	private static JMSMessageHandler jmsMessageHandler;
	private InitialContext ctx = null;
	private QueueConnectionFactory qcf = null;
	private QueueConnection qc = null;
	private QueueSession qsess = null;
	private Queue q = null;
	private QueueSender qsndr = null;
	private TextMessage message = null;
	private String QCF_NAME;
	private String QUEUE_NAME;
	private String URL;
	private String USER;
	private String PASSWORD;
	private Properties properties = null;

	public JMSMessageHandler() {
		super();
	}

	public static JMSMessageHandler init(Properties p) {
		if (jmsMessageHandler == null) {
			jmsMessageHandler = new JMSMessageHandler();
			jmsMessageHandler.getProperties(p);
		}
		return jmsMessageHandler;
	}

	private void getProperties(Properties p) {
		properties = p;
		QCF_NAME = p.getProperty("QCF_NAME");
		QUEUE_NAME = p.getProperty("QUEUE_NAME");
		URL = p.getProperty("URL");
		USER = p.getProperty("USER");
		PASSWORD = p.getProperty("PASSWORD");
	}

	public void sendTextMessage(String messageText) throws JMSException,
			WrongServerConfigurationException {
		sendTextMessage(URL, USER, PASSWORD, QCF_NAME, QUEUE_NAME, messageText);
	}

	private void sendTextMessage(String url, String user, String password,
			String cf, String queue, String messageText) throws JMSException,
			WrongServerConfigurationException {
		// create InitialContext
		Hashtable<String, String> properties = new Hashtable<String, String>();
		properties.put(Context.INITIAL_CONTEXT_FACTORY,
				"weblogic.jndi.WLInitialContextFactory");
		properties.put(Context.PROVIDER_URL, url);
		properties.put(Context.SECURITY_PRINCIPAL, user);
		properties.put(Context.SECURITY_CREDENTIALS, password);

		try {
			ctx = new InitialContext(properties);
		} catch (NamingException e) {
			throw new WrongServerConfigurationException(
					"Failed to initialize context. Server is stopped or properties are wrong.\nPlease check that the server is running and that your configuration is correct.\n Properties found :"
							+ properties);
		}
		try {
			try {
				qcf = (QueueConnectionFactory) ctx.lookup(cf);
			} catch (NamingException e) {
				throw new WrongServerConfigurationException(
						"Failed to find Connection factory. Either CF hasn't been created or jndi name in properties is not the correct one.\nConnection Factory jndi name: "
								+ cf);
			}
			qc = qcf.createQueueConnection();
			qsess = qc.createQueueSession(false, 0);
			try {
				q = (Queue) ctx.lookup(queue);
			} catch (NamingException e) {
				throw new WrongServerConfigurationException(
						"Failed to find Queue. Either queue hasn't been created or jndi name in properties is not the correct one.\nQueue jndi name: "
								+ cf);

			}
			qsndr = qsess.createSender(q);
			message = qsess.createTextMessage();
			message.setText(messageText);
			qsndr.send(message);
		} catch (JMSException jmse) {
			throw jmse;
		} finally {
			message = null;
			qsndr.close();
			qsndr = null;
			q = null;
			qsess.close();
			qsess = null;
			qc.close();
			qc = null;
			qcf = null;
			ctx = null;
		}
	}

}
