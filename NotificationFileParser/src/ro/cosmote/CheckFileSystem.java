package ro.cosmote;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.X509Certificate;
import java.util.Properties;

import javax.jms.JMSException;
import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ro.cosmote.exception.WrongServerConfigurationException;
import ro.cosmote.util.JMSMessageHandler;

//Need to add Logging

public class CheckFileSystem {
	private Properties properties;
	private String propertiesLocation;
	private JMSMessageHandler jmsHandler = new JMSMessageHandler();
	private static Logger log = Logger.getRootLogger();

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		try {
			if (args == null || args.length < 1) {
				log.fatal("NO ARGUMENTS PROVIDED");
				System.exit(0);
			}
			
			CheckFileSystem cfs = new CheckFileSystem();
			// Get the properties location from the input
			cfs.propertiesLocation = args[0];
			cfs.init();
			cfs.processFiles();
		} catch (Exception e) {
			log.fatal("Application did not finish successfully"+e.getMessage(), e.getCause());
			e.printStackTrace();
		}

	}

	private void init() throws Exception {

		properties = new Properties();
		FileInputStream in;
		try {
			in = new FileInputStream(this.propertiesLocation);
			properties.load(in);
			in.close();
			jmsHandler = JMSMessageHandler.init(properties);
		} catch (FileNotFoundException fnfe) {
			log.error("Property File Not Found.[" + this.propertiesLocation
					+ "]");
			throw fnfe;
		} catch (IOException ioe) {
			throw ioe;
		}

	}

	private void processFiles() {
		String inputFolderName = this.properties.getProperty("root_folder")
				+ System.getProperty("file.separator")
				+ this.properties.getProperty("input_folder");
		File inputFolder = new File(inputFolderName);
		File errorFolder = new File(this.properties.getProperty("root_folder")
				+ System.getProperty("file.separator")
				+ this.properties.getProperty("error_folder"));
		try {
			if (inputFolder.isDirectory()) {
				File[] files = inputFolder.listFiles();
				int filesCounter = 0;
				for (File f : files) {

					if (f.isDirectory()) {

						FileUtils
								.moveDirectoryToDirectory(f, errorFolder, true);

						log.error("Directory "
								+ f.getName()
								+ " found inside "
								+ inputFolder.getName()
								+ ". Directories are not supported and this has been moved to ["
								+ errorFolder.getPath() + "]. "
								+ "Please copy only text files.");
						 f.deleteOnExit();
					} else {

						if (validateFile(f)) {
							String newUniqueFileName = this.properties
									.getProperty("root_folder")
									+ System.getProperty("file.separator")
									+ this.properties
											.getProperty("processing_folder")
									+ System.getProperty("file.separator")
									+ f.getName()
									+ "_"
									+ String.valueOf(System.currentTimeMillis())
									+ "_" + String.valueOf(filesCounter);
							File fileForProcessing = new File(newUniqueFileName);
							FileUtils.copyFile(f, fileForProcessing);

							// Put a JMS message in the queue
							try {
								jmsHandler.sendTextMessage(newUniqueFileName);
								f.deleteOnExit();
							} catch (JMSException e) {
								log.error("FAILED TO SEND MESSAGE TO THE QUEUE. CONTACT YOUR VENDOR");
								e.printStackTrace();
								fileForProcessing.deleteOnExit();
							} catch (WrongServerConfigurationException e) {
								log.error(e.getMessage()
										+ " CONTACT YOUR VENDOR");
								fileForProcessing.deleteOnExit();
							}

						} else { // If file is not validated then put it in the
									// error folder
							log.info("File "
									+ f.getName()
									+ " IS NOT VALID. Please review file format");
							FileUtils
									.copyFile(
											f,
											new File(
													errorFolder
															+ System.getProperty("file.separator")
															+ f.getName()));
							f.deleteOnExit();
						}
					}
				}
			} else {
				log.error("Input Folder provided is not a directory.\nInput folder path provided is: \n\t"
						+ inputFolderName);
			}
		} catch (IOException e) {
			log.error(e.getMessage(), e.getCause());
		}
	}

	private boolean validateFile(File f) {
		FileInputStream fis = null;
		BufferedReader br = null;
		try {
			fis = new FileInputStream(f);

			// Construct BufferedReader from InputStreamReader
			br = new BufferedReader(new InputStreamReader(fis));

			String line = null;
			int lineNr = 0;
			int numOfLines = 0;
			int fileType = 0;
			log.info("START VAILDATING FILE-["+f.getName()+"]");
			while ((line = br.readLine()) != null) {
				if (lineNr == 0) {
					if(line != null && line.length()>4){
						String[] tokens = line.split("#");
						numOfLines = Integer.parseInt(tokens[2]);
						fileType = Integer.parseInt(tokens[1]);
						lineNr++;
						if (fileType == 1
								&& (tokens.length == 2)) {
							log.info("FILE:["+f.getName()+"]-File type provided in the file requires for a payload message in the header. This file is not valid");
							return false;
						}
					}else{
						log.info("FILE:["+f.getName()+"]-File header hasn't been filled or file is empty");
						return false;
					}
				} else {
					lineNr++;
				}

			}
			log.info("FILE VALIDATION FINISHED-["+f.getName()+"]");
			if (numOfLines != lineNr) {
				log.info("FILE:["+f.getName()+"]- Number of lines in the header is not equal to the number of lines that actually exist in the file. File is not valid");
				return false;
			}
			

		} catch (IOException ioe) {
			return false;

		} finally {
			if (null != br)
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
		}
		return true;

	}

}
