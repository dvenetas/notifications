package ro.cosmote.exception;

import java.io.Serializable;

public class WrongServerConfigurationException 	extends Exception implements Serializable {

		public WrongServerConfigurationException() {
		}

		public WrongServerConfigurationException(String pExceptionMsg) {
			super(pExceptionMsg);
		}

		public WrongServerConfigurationException(String pExceptionMsg, Throwable pException) {
			super(pExceptionMsg, pException);

		}
	
}
