package ro.telekom.log;

import java.util.Enumeration;

import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class CustomLogger {
	static Logger logger = null;
	
	private CustomLogger(){
		
	}
	
	public static synchronized Logger getInstance(){
		if(logger == null){
			PropertyConfigurator.configure(System.getProperty("user.dir")+System.getProperty("file.separator")+"propertyFiles"+System.getProperty("file.separator")+"log4j.properties");
			logger = Logger.getRootLogger();
			System.out.println("INIT log4j PROPERTIES");
		}
		return logger;
	}

	public static synchronized void refresh(){
		try{
		if(logger != null){
			System.out.println("MyLogger");
			if(logger.getLoggerRepository() != null){
				System.out.println("MyRepository");
				logger.getLoggerRepository().shutdown();
			}else{
				System.out.println("MyRepository is null");
			}
			logger = null;
		}else{
			System.out.println("MyLogger is null");
		}
		
		LogManager.shutdown();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public synchronized static Logger createCustomLogger(String fileName) {
//		rootLogger.info("\n**************Log file for this run: " + "Notification_"+getFileID()+".log"
//				+ "\n**************\n");
		Logger rootLogger=getInstance();
		return rootLogger;
//		Enumeration appenders = rootLogger.getAllAppenders();
//		FileAppender fa = null;
//		while (appenders.hasMoreElements()) {
//			Appender currAppender = (Appender) appenders.nextElement();
//			if (currAppender instanceof FileAppender) {
//				fa = (FileAppender) currAppender;
//			}
//		}
//		if (fa != null) {
//			fa.setFile("logs/Notification_"+fileName+".log");
//			fa.activateOptions();
//		} else {
////			logger.info("No File Appender found");
//		}
//		return rootLogger;
	}
	
	
}
