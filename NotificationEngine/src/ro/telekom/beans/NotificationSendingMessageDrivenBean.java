package ro.telekom.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

import ro.telekom.beans.timer.ejb.TimerServiceSendNotification;
import ro.telekom.exception.JDBCConnectionException;
import ro.telekom.exception.MissingPropertyFileException;
import ro.telekom.exception.NotificationProcessingException;
import ro.telekom.log.CustomLogger;
import ro.telekom.push.apns.APNSPush;
import ro.telekom.push.gcm.GCMPush;
import ro.telekom.util.JDBCConnectionHandler;
import ro.telekom.util.JMSMessageHandler;
import ro.telekom.util.JMSNotificationResponseMessageHandler;
import ro.telekom.value.FileInfo;
import ro.telekom.value.GenericProperties;
import ro.telekom.value.NotificationBatchResponse;
import ro.telekom.value.NotificationResponse;
import ro.telekom.value.SQLProperties;
import ro.telekom.value.enumerator.NotificationStatus;
import ro.telekom.value.enumerator.OSType;
import ro.telekom.value.enumerator.PayloadType;

@MessageDriven(messageListenerInterface = javax.jms.MessageListener.class, name = "NotificationSendingMessageDrivenBean", mappedName = "jms/queue/NotificationSendingJMSQueue", activationConfig = {
		@ActivationConfigProperty(propertyName = "connectionFactoryJndiName", propertyValue = "jms/cf/NotificationFilesConnectionFactory"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue") })
@TransactionManagement(TransactionManagementType.CONTAINER)
public class NotificationSendingMessageDrivenBean implements MessageListener,
		ExceptionListener {

	@Resource
	private MessageDrivenContext context;
	@EJB
	private TimerServiceSendNotification timer;
	private int fileID;
	private FileInfo fileInfo = new FileInfo();
	private Connection connection;
	private Integer maxCount;
	private boolean completed;
	private boolean rollback = false;
	private List<Long> IDs = new ArrayList<Long>();
	Logger logger = CustomLogger.getInstance();
	String logId;

	@Override
	public void onException(JMSException arg0) {

	}

	@Override
	public void onMessage(Message message) {
		try {
			if (message instanceof TextMessage) {
				initDB();

				setCompleted(false);
				setFileAndProcessId(((TextMessage) message).getText());

				logger.info("[" + logId
						+ "] NotificationSendingMessageDrivenBean: File id: "
						+ getFileID() + " starting processing..");

				String startFrom = retrieveProperty("startFrom");
				String endTo = retrieveProperty("endTo");
				setMaxCount(Integer.parseInt(retrieveProperty("maxCount")));
				logger.debug("[" + logId + "] maxCount: " + getMaxCount());
				long timeFrame = calculateValidProcessingFrame(startFrom, endTo);
				if (timeFrame == 0) {
					// Retrieve pending notifications for pushing
					setFileInfo(retrieveFileInfo(getFileID()));
					if (fileInfo != null && fileInfo.getFileName() != null) {
						logger.info("[" + logId + "] FileInfo payloadType: "
								+ fileInfo.getFileType() + ". messageText "
								+ fileInfo.getPayload());
						NotificationBatchResponse batchResponse = new NotificationBatchResponse();
						batchResponse = sendPayloadApple(fileInfo.getPayload(),
								fileInfo.getFileType());
						logger.debug("["
								+ logId
								+ "] sendPayloadApple batchResponseApple deviceIds: "
								+ batchResponse.getDeviceIds());
						putMessageForUpdateLineStatus(batchResponse);
						if (!isCompleted()) {
							NotificationBatchResponse batchResponseG = sendPayloadGoogle(
									fileInfo.getPayload(),
									fileInfo.getFileType());
							logger.debug("[" + logId
									+ "] sendPayloadGoogle deviceIds: "
									+ batchResponseG.getDeviceIds());
							putMessageForUpdateLineStatus(batchResponseG);
							if (!isCompleted()) {
								logger.info("["
										+ logId
										+ "] Sending FileID for the rest deviceIds..");
								putMessageOnQueue("NOTIFICATION_SENDING_QUEUE_NAME");
							}
						}

					} else {
						logger.info("[" + logId + "] FileID : " + getFileID()
								+ " did not found on db.. please check!");
					}
				} else {
					logger.info("["
							+ logId
							+ "] NotificationSendingMessageDrivenBean will receive mesaage againg in "
							+ timeFrame + " ms!");
					String tmpName = "NotificationSendingTimer-"
							+ getFileID()
							+ "-"
							+ GenericProperties.getInstance().getProperty(
									"NOTIFICATION_SENDING_QUEUE_NAME")
							+ "-"
							+ GenericProperties.getInstance().getProperty(
									"QCF_NAME");
					logger.debug("[" + logId + "] timerName " + tmpName);
					timer.createTimer(tmpName, timeFrame, logId);
				}

			} else {
				logger.fatal("["
						+ logId
						+ "] Not valid message for NotificationSendingMessageDrivenBean.");
			}
		} catch (NotificationProcessingException ne) {
			logger.fatal("["
					+ logId
					+ "] [NotificationSendingMessageDrivenBean] NotificationProcessingException FAILED : "
					+ ne.getMessage());
			setRollback(ne.isRollback());
		} catch (JMSException e) {
			logger.fatal("["
					+ logId
					+ "] [NotificationSendingMessageDrivenBean] JMSException : "
					+ e.getMessage());
			setRollback(true);
		} catch (MissingPropertyFileException mpex) {
			logger.fatal("["
					+ logId
					+ "] [NotificationSendingMessageDrivenBeane] MissingPropertyFileException : "
					+ mpex.getMessage());
			setRollback(true);
		} finally {

			if (isRollback()) {
				// context.setRollbackOnly();
				try {
					updateLineStatusByStep(getIDs(),
							NotificationStatus.INSERTED.getStatus());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NotificationProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	private void setFileAndProcessId(String message) {
		String inputMessageArray[] = message.split("#<#");

		setFileID(Integer.parseInt(inputMessageArray[0]));
		if (inputMessageArray.length > 1) {
			logId = inputMessageArray[1];
		} else {
			logId = "dummy_" + getFileID();
		}
	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private NotificationBatchResponse sendPayloadGoogle(String messageText,
			int payloadType) throws NotificationProcessingException {
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		NotificationBatchResponse batchResponse = new NotificationBatchResponse();
		try {
			List<NotificationResponse> responses = new ArrayList<NotificationResponse>();
			prepStmt = connection.prepareStatement(SQLProperties.getInstance()
					.getProperty("SELECT.BATCH.PROCESSING"));
			prepStmt.setInt(1, getFileID());
			prepStmt.setInt(2, OSType.ANDROID.getOSType());
			prepStmt.setInt(3, NotificationStatus.INSERTED.getStatus());
			prepStmt.setInt(4, getMaxCount() / 2);
			rs = prepStmt.executeQuery();
			if (rs != null && rs.next()) {
				List<String> deviceIdsGoogle = new LinkedList<String>();
				List<Long> ids = new LinkedList<Long>();
				deviceIdsGoogle.add(rs.getString("INSTALLATION_ID"));
				ids.add(rs.getLong("ID"));
				if (payloadType == PayloadType.SINGLE.getPayloadType()) {
					while (rs.next()) {
						deviceIdsGoogle.add(rs.getString("INSTALLATION_ID"));
						ids.add(rs.getLong("ID"));
					}
					logger.info("[" + logId
							+ "] [sendPayloadGoogle] updateLineStatus UNKOWN");
					setIDs(ids);
					updateLineStatusByStep(ids,
							NotificationStatus.UNKOWN.getStatus());
					GCMPush gcm = new GCMPush(String.valueOf(getFileID()),
							logId);
					responses = gcm.sendPush(messageText, deviceIdsGoogle);
				} else {
					HashMap<String, String> messageDeviceMap = new HashMap<String, String>();
					messageDeviceMap.put(rs.getString("INSTALLATION_ID"),
							rs.getString("LINE_PAYLOAD"));
					deviceIdsGoogle.add(rs.getString("INSTALLATION_ID"));
					ids.add(rs.getLong("ID"));
					while (rs.next()) {
						messageDeviceMap.put(rs.getString("INSTALLATION_ID"),
								rs.getString("LINE_PAYLOAD"));
						deviceIdsGoogle.add(rs.getString("INSTALLATION_ID"));
						ids.add(rs.getLong("ID"));
					}
					setIDs(ids);
					updateLineStatusByStep(ids,
							NotificationStatus.UNKOWN.getStatus());
					logger.info("[" + logId
							+ "] [sendPayloadGoogle] updateLineStatus UNKOWN");
					GCMPush gcm = new GCMPush(String.valueOf(getFileID()),
							logId);
					responses = gcm.sendPushPerDevice(messageDeviceMap);
				}
				batchResponse.setDeviceIds(deviceIdsGoogle);
				batchResponse.setNotificationResponses(responses);
				logger.debug("[" + logId
						+ "] [sendPayloadGoogle] batchResponse: "
						+ batchResponse);

			} else {
				setCompleted(checkForFinished(OSType.APPLE.getOSType()));
			}
		} catch (SQLException e) {
			throw new NotificationProcessingException(e.getMessage()
					+ "Cannot sendPayloadGoogle : ", false, e.getCause());
		} catch (Exception ex) {
			throw new NotificationProcessingException(ex.getMessage()
					+ "Cannot sendPayloadGoogle : ", false, ex.getCause());
		} finally {
			closeDB(prepStmt, rs);
		}
		return batchResponse;
	}

	/**
	 * Retrieves ios inserted and start processing
	 * 
	 * @param messageText
	 * @throws NotificationProcessingException
	 */
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private NotificationBatchResponse sendPayloadApple(String messageText,
			int payloadType) throws NotificationProcessingException {
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		NotificationBatchResponse batchResponse = new NotificationBatchResponse();
		logger.info("[" + logId + "] [sendPayloadApple] Starting..");
		try {
			List<NotificationResponse> responses = new ArrayList<NotificationResponse>();
			prepStmt = connection.prepareStatement(SQLProperties.getInstance()
					.getProperty("SELECT.BATCH.PROCESSING"));
			logger.info("["
					+ logId
					+ "] [sendPayloadApple] sql=: "
					+ SQLProperties.getInstance().getProperty(
							"SELECT.BATCH.PROCESSING"));
			prepStmt.setInt(1, getFileID());
			prepStmt.setInt(2, OSType.APPLE.getOSType());
			prepStmt.setInt(3, NotificationStatus.INSERTED.getStatus());
			prepStmt.setInt(4, getMaxCount() / 2);
			rs = prepStmt.executeQuery();
			// HashMap<Long, String> idInstallationIds= new HashMap<Long,
			// String>();
			List<String> deviceIdsApple = new LinkedList<String>();
			List<Long> ids = new LinkedList<Long>();
			if (rs != null && rs.next()) {
				deviceIdsApple.add(rs.getString("INSTALLATION_ID"));
				ids.add(rs.getLong("ID"));

				logger.debug("["
						+ logId
						+ "] [sendPayloadApple] Prepare to send push notifications in Apple for payloadtype "
						+ payloadType);
				if (payloadType == PayloadType.SINGLE.getPayloadType()) {
					while (rs.next()) {
						deviceIdsApple.add(rs.getString("INSTALLATION_ID"));
						ids.add(rs.getLong("ID"));
					}
					updateLineStatusByStep(ids,
							NotificationStatus.UNKOWN.getStatus());
					setIDs(ids);
					APNSPush apns = new APNSPush(String.valueOf(getFileID()),
							logId);
					logger.info("[" + logId
							+ "] [sendPayloadApple]  [APNSPush] messageText : "
							+ messageText);
					logger.info("["
							+ logId
							+ "] [sendPayloadApple]  [APNSPush] deviceIdsApple size : "
							+ deviceIdsApple.size());
					responses = apns.sendPush(messageText, deviceIdsApple);
				} else if (payloadType == PayloadType.MULTIPLE.getPayloadType()) {
					HashMap<String, String> messageDeviceMap = new HashMap<String, String>();
					messageDeviceMap.put(rs.getString("INSTALLATION_ID"),
							rs.getString("LINE_PAYLOAD"));
					ids.add(rs.getLong("ID"));
					deviceIdsApple.add(rs.getString("INSTALLATION_ID"));
					while (rs.next()) {
						messageDeviceMap.put(rs.getString("INSTALLATION_ID"),
								rs.getString("LINE_PAYLOAD"));
						ids.add(rs.getLong("ID"));
						deviceIdsApple.add(rs.getString("INSTALLATION_ID"));
					}
					setIDs(ids);

					updateLineStatusByStep(ids,
							NotificationStatus.UNKOWN.getStatus());
					logger.info("["
							+ logId
							+ "] [sendPayloadApple]  [APNSPush] messageDeviceMap size: "
							+ messageDeviceMap.entrySet().size());
					APNSPush apns = new APNSPush(String.valueOf(getFileID()),
							logId);
					responses = apns.sendPushPerDevice(messageDeviceMap);
				} else {
					logger.fatal("[" + logId
							+ "] [sendPayloadApple] no valid payloadType ");

				}
				logger.info("["
						+ logId
						+ "] [sendPayloadApple]  [APNSPush] push notifications ended.");
				batchResponse.setDeviceIds(deviceIdsApple);
				batchResponse.setNotificationResponses(responses);
			} else {
				logger.debug("["
						+ logId
						+ "] s[sendPayloadApple] Not ios type Found in db. os type ="
						+ OSType.APPLE.getOSType());
				setCompleted(checkForFinished(OSType.ANDROID.getOSType()));
			}
		} catch (NotificationProcessingException nex) {
			throw new NotificationProcessingException(nex.getMessage()
					+ "Cannot sendPayloadApple : ", nex.isRollback(),
					nex.getCause());
		} catch (SQLException e) {
			throw new NotificationProcessingException(e.getMessage()
					+ "Cannot sendPayloadApple : ", false, e.getCause());
		} catch (Exception ex) {
			throw new NotificationProcessingException(ex.getMessage()
					+ "Cannot sendPayloadApple : ", false, ex.getCause());
		} finally {
			closeDB(prepStmt, rs);
		}
		logger.info("[" + logId + "] [sendPayloadApple] Finished successfully.");
		return batchResponse;
	}

	/**
	 * for each response.getDeviceId record will be updated with the
	 * corresponsing status
	 * 
	 * @param responses
	 * @param deviceIds
	 * @throws NotificationProcessingException
	 */

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private void updateLineStatusByStep(List<Long> ids, int status)
			throws SQLException, NotificationProcessingException {
		try {
			logger.info("[" + logId + "] [updateLineStatusByStep] "
					+ getFileID() + " Starting... ");
			String devicesIdsStr = "";
			int counting = (ids.size() / 500);
			int mod = ids.size() % 500;
			int counter = 0;
			Iterator it = ids.iterator();
			logger.debug("[" + logId + "] [updateLineStatusByStep] "
					+ getFileID() + " counting: " + counting);
			for (int i = 0; i < counting; i++) {
				counter = 0;
				devicesIdsStr = "";
				while (it.hasNext()) {
					if (counter >= 500)
						break;
					devicesIdsStr += "," + it.next();
					it.remove();
					counter++;
				}
				devicesIdsStr = devicesIdsStr.replaceFirst(",", "");

				logger.debug("[" + logId + "] [updateLineStatusByStep] "
						+ getFileID() + " devicesIdsStr: " + devicesIdsStr
						+ " status : " + status);
				int updates = updateLinesStatus(devicesIdsStr, status);
				logger.debug("[" + logId + "] [updateLineStatusByStep] "
						+ getFileID() + " for counting: [" + i + "] from ["
						+ counting + "] --> updateLinesStatus: " + updates);
			}
			if (mod > 0) {
				devicesIdsStr = "";
				while (it.hasNext()) {
					devicesIdsStr += "," + it.next();
					it.remove();
				}
				devicesIdsStr = devicesIdsStr.replaceFirst(",", "");
				logger.debug("[" + logId + "] [updateLineStatusByStep] "
						+ getFileID() + " LAST UPDATE " + mod + " rows.");
				logger.debug("[" + logId + "] [updateLineStatusByStep] "
						+ getFileID() + " devicesIdsStr: " + devicesIdsStr
						+ " status : " + status);
				int updates = updateLinesStatus(devicesIdsStr, status);
				logger.debug("[" + logId + "] [updateLineStatusByStep] "
						+ getFileID() + " for mod: " + mod
						+ " --> updateLinesStatus: " + updates);
			}
		} catch (Exception ex) {
			throw new NotificationProcessingException(ex.getMessage()
					+ "[updateLineStatusByStep] FAILED  : " + getFileID(),
					true, ex.getCause());
		}

	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private int updateLinesStatus(String idsStr, int status)
			throws SQLException, NotificationProcessingException {
		int updates = 0;
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		logger.info("[" + logId + "] [updateLineStatus] " + getFileID()
				+ " Starting... ");
		logger.debug("[" + logId + "] [updateLineStatus] " + getFileID()
				+ " devicesIdsStr: " + idsStr + " status : " + status);
		try {
			if (!idsStr.equals("")) {
				String query = SQLProperties.getInstance().getProperty(
						"UPDATE.FILE.LINES.BY.ID")
						+ idsStr + ")";
				logger.debug("[" + logId + "] [updateLineStatus] "
						+ getFileID() + " query: " + query);
				prepStmt = connection.prepareStatement(query);
				prepStmt.setInt(1, status);
				updates = prepStmt.executeUpdate();
				connection.commit();
			}
		} catch (SQLException e) {
			throw new NotificationProcessingException(e.getMessage()
					+ "[updateLineStatus] FAILED  : " + getFileID(), true,
					e.getCause());
		} catch (Exception ex) {
			throw new NotificationProcessingException(ex.getMessage()
					+ "[updateLineStatus] FAILED  : " + getFileID(), true,
					ex.getCause());
		} finally {
			closeDB(prepStmt, rs);
		}
		return updates;
	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private String retrieveProperty(String property)
			throws NotificationProcessingException {
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		String value = "";
		try {
			logger.debug("[" + logId + "] [retrieveProperty]: " + property);
			prepStmt = connection.prepareStatement(SQLProperties.getInstance()
					.getProperty("SELECT.PROPERTY.BY.ID"));
			prepStmt.setString(1, property);
			rs = prepStmt.executeQuery();
			if (rs != null && rs.next()) {
				value = rs.getString(1);// 09:00 format
			}

		} catch (SQLException e) {
			throw new NotificationProcessingException(e.getMessage()
					+ "SQLException Cannot retrieveProperty fileId : "
					+ getFileID(), false, e.getCause());
		} catch (MissingPropertyFileException mpfe) {
			throw new NotificationProcessingException(
					mpfe.getMessage()
							+ " MissingPropertyFileException retrieveProperty failed: ",
					false, mpfe.getCause());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeDB(prepStmt, rs);
		}
		return value;
	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private boolean checkForFinished(int osType)
			throws NotificationProcessingException {
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		try {
			logger.info("[" + logId + "] [checkForFinished]: os " + osType);
			prepStmt = connection.prepareStatement(SQLProperties.getInstance()
					.getProperty("SELECT.BATCH.PROCESSING"));
			prepStmt.setInt(1, getFileID());
			prepStmt.setInt(2, osType);// @TODO prepei na vgei mazi kai sto
										// query
			prepStmt.setInt(3, NotificationStatus.INSERTED.getStatus());
			prepStmt.setInt(4, getMaxCount());
			rs = prepStmt.executeQuery();
			if (rs != null && rs.next()) {
				logger.info("[" + logId + "] [checkForFinished]: "
						+ getFileID() + " false");
				return false;
			}
		} catch (SQLException e) {
			throw new NotificationProcessingException(e.getMessage()
					+ "Cannot checkForFinished fileId : " + getFileID(), false,
					e.getCause());
		} catch (Exception ex) {
			throw new NotificationProcessingException(ex.getMessage()
					+ "Cannot checkForFinished fileId : " + getFileID(), false,
					ex.getCause());
		} finally {
			System.out.println();
			closeDB(prepStmt, rs);
		}
		logger.info("[" + logId + "] [checkForFinished]: " + getFileID()
				+ " true");
		return true;

	}

	/**
	 * returns true if current time is between from and to
	 * 
	 * @param startFrom
	 * @param startTo
	 * @return
	 * @throws NotificationProcessingException
	 */
	private long calculateValidProcessingFrame(String startFrom, String endTo)
			throws NotificationProcessingException {
		try {
			logger.info("[" + logId + "] [calculateValidProcessingFrame] for "
					+ this.fileID + " entered..");

			// String startFrom = "21:00";
			// String endTo = "23:50";
			Calendar cal = Calendar.getInstance();

			String[] parts = startFrom.split(":");
			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
			cal1.set(Calendar.MINUTE, Integer.parseInt(parts[1]));

			parts = endTo.split(":");
			Calendar cal2 = Calendar.getInstance();
			cal2.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
			cal2.set(Calendar.MINUTE, Integer.parseInt(parts[1]));

			if (cal.before(cal2)) {
				logger.debug("[" + logId + "] Before " + cal2.getTime());
			} else {
				cal1.add(Calendar.DATE, 1);
				long reschedule = cal1.getTimeInMillis()
						- cal.getTimeInMillis();
				logger.debug("[" + logId + "] reschedule in ms : " + reschedule);
				return reschedule;
			}
			if (cal.after(cal1)) {
				logger.debug("[" + logId + "] After " + cal1.getTime());
			} else {
				long reschedule = cal1.getTimeInMillis()
						- cal.getTimeInMillis();
				logger.debug("[" + logId + "] reschedule in ms : " + reschedule);
				return reschedule;
			}
		} catch (Exception ex) {
			throw new NotificationProcessingException(
					getFileID()
							+ " [calculateValidProcessingFrame]: An error occured while calculating valid timeframe..",
					true, ex.getCause());
		}
		return 0;

	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private FileInfo retrieveFileInfo(final int fileId)
			throws NotificationProcessingException {
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		fileInfo.setId(fileId);
		try {
			logger.info("[" + logId
					+ "] [retrieveFileInfo]: Retrieve File Info by id "
					+ fileId);
			prepStmt = connection.prepareStatement(SQLProperties.getInstance()
					.getProperty("SELECT.FILE_INFO"));// ID,NAME, FILE_TYPE,
														// PAYLOAD
			prepStmt.setInt(1, getFileID());
			rs = prepStmt.executeQuery();
			if (rs != null && rs.next()) {
				logger.debug("[retrieveFileInfo]: Retrieved!");
				fileInfo.setFileName(rs.getString("FILE_NAME"));
				fileInfo.setPayload(rs.getString("PAYLOAD"));
				fileInfo.setFileType(rs.getInt("FILE_TYPE"));
			}
		} catch (SQLException e) {
			throw new NotificationProcessingException(e.getMessage()
					+ "Cannot retrieveFileInfo fileId : " + getFileID(), true,
					e.getCause());
		} catch (Exception ex) {
			throw new NotificationProcessingException(ex.getMessage()
					+ "Cannot retrieveFileInfo fileId : " + getFileID(), true,
					ex.getCause());
		} finally {
			closeDB(prepStmt, rs);
		}
		return fileInfo;

	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private void initDB() throws NotificationProcessingException {
		if (logger.isDebugEnabled())
			logger.debug("[" + logId
					+ "] NotificationSendingMessageDrivenBean initDB starts");
		try {
			JDBCConnectionHandler jdbc = new JDBCConnectionHandler();
			connection = jdbc.getDefaultDBConn();
			if (logger.isDebugEnabled())
				logger.debug("[" + logId
						+ "] NotificationSendingMessageDrivenBean initDB ends");
		} catch (JDBCConnectionException e) {
			throw new NotificationProcessingException(e.getMessage()
					+ "initDB() FAILED : ", true, e.getCause());
		} catch (Exception ex) {
			throw new NotificationProcessingException(ex.getMessage()
					+ "Cannot initDB fileId : " + getFileID(), true,
					ex.getCause());
		}

	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private void closeDB(PreparedStatement prepStmt, ResultSet rs)
			throws NotificationProcessingException {
		if (logger.isDebugEnabled())
			logger.debug("["
					+ logId
					+ "] NotificationUpdateLineStatusMessageDrivenBean closeDB starts");
		try {
			if (prepStmt != null) {
				try {
					prepStmt.close();
				} catch (Exception e) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
			}
			if (logger.isDebugEnabled())
				logger.debug("["
						+ logId
						+ "] NotificationUpdateLineStatusMessageDrivenBean closeDB ends");
		} catch (Exception ex) {
			throw new NotificationProcessingException(ex.getMessage()
					+ " closeDB() FAILED: ", false, ex.getCause());
		}

	}

	/**
	 * Puts the message of fileId back so as the notification mechanism starts
	 * again in order continue with the rest records...
	 * 
	 * @throws MissingPropertyFileException
	 */
	public void putMessageOnQueue(String queueNameProperty)
			throws NotificationProcessingException {
		try {
			JMSMessageHandler jms = JMSMessageHandler.init();
			jms.sendTextMessage(
					String.valueOf(getFileID()) + "#<#" + logId,
					GenericProperties.getInstance().getProperty(
							queueNameProperty), GenericProperties.getInstance()
							.getProperty("QCF_NAME"));
		} catch (MissingPropertyFileException mpfe) {
			throw new NotificationProcessingException(mpfe.getMessage()
					+ " putMessageOnQueue failed: ", false, mpfe.getCause());
		}
	}

	private void putMessageForUpdateLineStatus(
			NotificationBatchResponse batchResponse)
			throws NotificationProcessingException {
		try {
			batchResponse.setFileId(getFileID());
			batchResponse.setProcessId(logId);
			logger.info("[" + logId + "] putMessageForUpdateLineStatus!");
			JMSNotificationResponseMessageHandler jnrmh = new JMSNotificationResponseMessageHandler();
			jnrmh.sendNotificationResponseMessage(
					batchResponse,
					GenericProperties.getInstance().getProperty(
							"NOTIFICATION_UPDATE_LINE_STATUS_QUEUE_NAME"),
					GenericProperties.getInstance().getProperty("QCF_NAME"));
		} catch (MissingPropertyFileException mpfe) {
			throw new NotificationProcessingException(mpfe.getMessage()
					+ " putMessageForUpdateLineStatus failed: ", false,
					mpfe.getCause());
		}
	}

	public int getFileID() {
		return fileID;
	}

	public void setFileID(int fileID) {
		this.fileID = fileID;
	}

	public Integer getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(Integer maxCount) {
		this.maxCount = maxCount;
	}

	public static void main(String[] args) {
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public boolean isRollback() {
		return rollback;
	}

	public void setRollback(boolean rollback) {
		this.rollback = rollback;
	}

	public List<Long> getIDs() {
		return IDs;
	}

	public void setIDs(List<Long> iDs) {
		IDs = iDs;
	}

	public FileInfo getFileInfo() {
		return fileInfo;
	}

	public void setFileInfo(FileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}
}
