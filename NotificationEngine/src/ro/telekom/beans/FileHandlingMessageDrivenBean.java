package ro.telekom.beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ro.telekom.exception.JDBCConnectionException;
import ro.telekom.exception.MissingPropertyFileException;
import ro.telekom.log.CustomLogger;
import ro.telekom.util.JDBCConnectionHandler;
import ro.telekom.util.JMSMessageHandler;
import ro.telekom.value.GenericProperties;
import ro.telekom.value.enumerator.PayloadType;
import ro.telekom.value.SQLProperties;
import ro.telekom.value.SubscriberDeviceInfo;
import ro.telekom.value.enumerator.NotificationStatus;

@MessageDriven(messageListenerInterface = javax.jms.MessageListener.class, name = "FileHandlingMessageDrivenBean", mappedName = "jms/queue/NotificationFilesJMSQueue", activationConfig = {
		@ActivationConfigProperty(propertyName = "connectionFactoryJndiName", propertyValue = "jms/cf/NotificationFilesConnectionFactory"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue") })
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FileHandlingMessageDrivenBean implements MessageListener,
		ExceptionListener {

	Logger logger = CustomLogger.getInstance();
	String logId;
	@Resource
	private MessageDrivenContext context;

	@Override
	public void onException(JMSException arg0) {

	}

	@Override
	public void onMessage(Message message) {
		
		String status = "SUCCESS";
		String filePath = null;
		File f = null;
		int groupId = 0;
		try {
			filePath = ((TextMessage) message).getText();
			String[] tokens = filePath.split("#<#");
			if(tokens.length>1){
				logId = tokens[1];
			}else{
				logId = UUID.randomUUID().toString();
			}
			logger.info("["+logId+"]"+"File: " + filePath + "found.");
			f = new File(filePath);
			int fileId = persistFileInDB(f, groupId);
			if (fileId != 0) {
				putMessageForSending(String.valueOf(fileId));
			}
		} catch (JMSException e) {
			logger.error("["+logId+"]"+
					"Failed to get the path from the JMS message. Exception: "
							+ e.getMessage(), e.getCause());
		} catch (SQLException e) {
			logger.error("["+logId+"]"+e.getMessage(), e.getCause());
			context.setRollbackOnly();
		} catch (MissingPropertyFileException e) {
			logger.error("["+logId+"]"+"A property File is missing: " + e.getMessage()
					+ ". Will not process this file any more.", e.getCause());
			try {
				String targetDir = f.getParentFile().getParent()
						+ System.getProperty("file.separator") + "error";
				FileUtils.moveFileToDirectory(f, new File(targetDir), true);
				logger.error("["+logId+"]"+"File has been moved to " + targetDir,
						e.getCause());
			} catch (IOException ioe) {
				logger.error("["+logId+"]"+"Failed to move file to error destination");
			}
		} catch (JDBCConnectionException e) {
			logger.error("["+logId+"]"+
					"Possible misconfiguration will not continue processing\n"
							+ e.getMessage(), e.getCause());
			try {
				String targetDir = f.getParentFile().getParent()
						+ System.getProperty("file.separator") + "error";
				FileUtils.moveFileToDirectory(f, new File(targetDir), true);
				logger.error("["+logId+"]"+"File has been moved to " + targetDir,
						e.getCause());
			} catch (IOException ioe) {
				logger.error("["+logId+"]"+"Failed to move file to error destination");
			}
		} catch (IOException e) {
			logger.error("["+logId+"]"+e.getMessage(), e.getCause());
			context.setRollbackOnly();
		} finally {
			logger.info("["+logId+"]"+"File: " + filePath + " result: " + status);
		}

	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private int persistFileInDB(File f, int groupId) throws SQLException,
			MissingPropertyFileException, IOException, JDBCConnectionException {
		Connection c = null;
		FileInputStream fis = null;
		BufferedReader br = null;
		int fileId = 0;
		try {
			c = (new JDBCConnectionHandler()).getDefaultDBConn();
			// Check that file is not already in DB. If file exists then the
			// fileId is returned and no database actions are performed.
			// The sending mdb will check if all notifications have been sent
			// and will continue to finish the file processing
			fileId = getFileIdFromFilePath(f.getPath(), c);
			try {
				fis = new FileInputStream(f);
				br = new BufferedReader(new InputStreamReader(fis));
			} catch (IOException e) {
				logger.error("["+logId+"]"+e.getMessage(), e.getCause());
				throw e;
			}

			logger.info("["+logId+"]"+"[fileId:"+fileId+"] "+f.getName() + ": START PARSING & INSERT IN DB");

			int maxCount = Integer.parseInt(retrieveProperty("maxCount", c));
			int existingLinesForFileId = getExistingLines(fileId, c);
			logger.info("["+logId+"]"+"[fileId:"+fileId+"] "+f.getName() + "ExistingLinesinDB=" + existingLinesForFileId);
			String line = null;
			int lineNr = 0;
			int fileType = 0;
			int cnt = 0;
			while ((line = br.readLine()) != null) {
				String payload = "";
				String[] tokens = line.split("#");

				if ((lineNr == 0) && (fileId == 0)) {
					fileType = Integer.parseInt(tokens[1]);
					if (fileType == PayloadType.SINGLE.getPayloadType()) {
						payload = tokens[3];
					}
					// Handle first Line
					String numOfLines = tokens[2];
					logger.debug("INSERT FILE: " + f.getName() + " in DB.");
					fileId = insertFileEntry(f.getName(), f.getPath(),
							fileType, Integer.parseInt(numOfLines), payload, c);
					logger.debug("File [" + f.getName() + "] has id [" + fileId
							+ "]");
				} else {
					if(lineNr == 0){
						fileType = Integer.parseInt(tokens[1]);
					}
					if ((!"EOF".equals(tokens[1]))
							&& (lineNr > existingLinesForFileId)) {
						logger.debug("1.1");
						List<SubscriberDeviceInfo> devicesInfo = getDeviceInfo(
								tokens[1], c);
						for (SubscriberDeviceInfo deviceInfo : devicesInfo) {
							if (cnt < maxCount) {

								insertLineEntry(
										fileId,
										tokens[0],
										tokens[1],
										deviceInfo.getInstallationId(),
										Integer.parseInt(deviceInfo.getOsType()),
										fileType == PayloadType.SINGLE
												.getPayloadType() ? null
												: tokens[2], c);
								cnt++;
							} else {
								putMessageForContinueInsertLines(f.getPath());
								logger.info("["+logId+"]"+"[fileId:"+fileId+"] "+f.getName() + "COMMITING AT LINE=" + lineNr);
								c.commit();
								return 0;
							}
						}

					}
				}

				lineNr++;

			}
			c.commit();
			logger.info("["+logId+"]"+"[fileId:"+fileId+"] "+f.getName() + ": FINISH PARSING & INSERT IN DB");
		} catch (IOException e) {
			c.rollback();
			logger.error("["+logId+"]"+e.getMessage(), e.getCause());
			throw e;
		} catch (SQLException e) {
			c.rollback();
			logger.error("["+logId+"]"+e.getMessage(), e.getCause());
			throw e;
		} finally {
			if (br != null) {
				br.close();
			}
			if (fis != null) {
				fis.close();
			}
			// for logging possible connection closing problem...
			try {
				c.close();
			} catch (Exception e) {
				logger.fatal("["+logId+"]"+"POSSIBLE CONNECTION LEAK");
			}
		}
		return fileId;
	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private int insertFileEntry(String fileName, String filePath, int fileType,
			int numberOfLines, String payload, Connection con)
			throws SQLException, MissingPropertyFileException {
		PreparedStatement stmt = null;
		try {
			String insertStmt = SQLProperties.getInstance().getProperty(
					"INSERT_FILE_ENTRY_IN_FILES_TABLE");
			int fileId = getFileId(con);
			stmt = con.prepareStatement(insertStmt);
			stmt.setInt(1, fileId);
			stmt.setString(2, fileName);
			stmt.setString(3, filePath);
			stmt.setInt(4, fileType);
			stmt.setInt(5, numberOfLines);
			stmt.setString(6, payload);
			stmt.executeUpdate();
			stmt.close();
			return fileId;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private void insertLineEntry(int fileId, String lineNumber, String msisdn,
			String installationId, int osType, String payload, Connection con)
			throws SQLException, MissingPropertyFileException {
		PreparedStatement stmt = null;
		try {
			String insertStmt = SQLProperties.getInstance().getProperty(
					"INSERT_LINE_ENTRY_IN_LINES_TABLE");
			stmt = con.prepareStatement(insertStmt);
			stmt.setInt(1, fileId);
			stmt.setInt(2, Integer.valueOf(lineNumber));
			stmt.setString(3, msisdn);
			stmt.setString(4, installationId);
			stmt.setInt(5, osType);
			stmt.setString(6, payload == null ? "" : payload);
			stmt.setInt(7, NotificationStatus.INSERTED.getStatus());
			stmt.executeUpdate();
			stmt.close();
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	private List<SubscriberDeviceInfo> getDeviceInfo(String msisdn,
			Connection con) throws SQLException, MissingPropertyFileException {
		ResultSet rs = null;
		PreparedStatement stmt = null;
		List<SubscriberDeviceInfo> info = new ArrayList<SubscriberDeviceInfo>();
		try {
			String insertStmt = SQLProperties.getInstance().getProperty(
					"SELECT_DEVICE_INFO");
			stmt = con.prepareStatement(insertStmt);
			stmt.setString(1, msisdn);
			
			rs = stmt.executeQuery();
			while (rs.next()) {
				SubscriberDeviceInfo device = new SubscriberDeviceInfo();
				device.setCustomerCode(rs.getString("CUSTOMER_CODE"));
				device.setInstallationId(rs.getString("INSTALLATION_ID"));
				device.setMsisdn(rs.getString("MSISDN"));
				device.setOsType(rs.getString("OS_TYPE"));
				info.add(device);
			}
			
		} catch(Exception e){
			e.printStackTrace();
		}finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
		return info;
	}

	private int getFileIdFromFilePath(String filePath, Connection con)
			throws SQLException, MissingPropertyFileException {
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
			String selectStmt = SQLProperties.getInstance().getProperty(
					"GET_FILE_ID_FROM_FILE_PATH");
			stmt = con.prepareStatement(selectStmt);
			stmt.setString(1, filePath);
			rs = stmt.executeQuery();

			while (rs.next()) {
				return rs.getInt(1);
			}
			return 0;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}

	}

	private int getFileId(Connection con) throws SQLException,
			MissingPropertyFileException {
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
			String insertStmt = SQLProperties.getInstance().getProperty(
					"SELECT_FILE_ID_FROM_SEQUENCE");
			stmt = con.prepareStatement(insertStmt);
			rs = stmt.executeQuery();
			rs.next();
			int seq = rs.getInt(1);
			stmt.close();
			rs.close();
			return seq;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	private int getExistingLines(int fileId, Connection con)
			throws SQLException, MissingPropertyFileException {
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
			String insertStmt = SQLProperties.getInstance().getProperty(
					"COUNT_LINES_FOR_FILE_ID");
			stmt = con.prepareStatement(insertStmt);
			stmt.setInt(1, fileId);
			rs = stmt.executeQuery();
			rs.next();
			int seq = rs.getInt(1);
			stmt.close();
			rs.close();
			return seq;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private String retrieveProperty(String property, Connection c)
			throws SQLException, MissingPropertyFileException {
		logger.debug("[retrieveProperty]: " + property);
		ResultSet rs = null;
		PreparedStatement prepStmt = null;
		String value = "";
		try {
			prepStmt = c.prepareStatement(SQLProperties.getInstance()
					.getProperty("SELECT.PROPERTY.BY.ID"));
			prepStmt.setString(1, property);
			rs = prepStmt.executeQuery();
			if (rs != null && rs.next()) {
				value = rs.getString(1);// 09:00 format
			}
			return value;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (prepStmt != null) {
				prepStmt.close();
			}
		}
	}

	private void putMessageForSending(String message)
			throws MissingPropertyFileException {

		message = message+"#<#"+logId;
		JMSMessageHandler.init().sendTextMessage(
				message,
				GenericProperties.getInstance().getProperty(
						"NOTIFICATION_SENDING_QUEUE_NAME"),
				GenericProperties.getInstance().getProperty("QCF_NAME"));
	}

	private void putMessageForContinueInsertLines(String message)
			throws MissingPropertyFileException {
		message = message+"#<#"+logId;
		JMSMessageHandler jms = JMSMessageHandler.init();
		jms.sendTextMessage(
				message,
				GenericProperties.getInstance().getProperty(
						"NOTIFICATION_FILES_QUEUE_NAME"),
				GenericProperties.getInstance().getProperty("QCF_NAME"));
//		jms.close();
	}
}