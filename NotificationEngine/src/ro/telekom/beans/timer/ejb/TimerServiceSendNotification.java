package ro.telekom.beans.timer.ejb;

import javax.ejb.Remote;

@Remote
public interface TimerServiceSendNotification {
	public void createTimer(String timerName);

	public void createTimer(String timerName, long expiration, String logId);

	public void closeTimer(String timerName);
}
