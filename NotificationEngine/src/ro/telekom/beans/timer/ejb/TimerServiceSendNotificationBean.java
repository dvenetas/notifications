package ro.telekom.beans.timer.ejb;

import static javax.ejb.TransactionAttributeType.REQUIRES_NEW;

import java.util.Collection;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;

import ro.telekom.exception.MissingPropertyFileException;
import ro.telekom.util.JMSMessageHandler;

@Stateless(name = "TimerServiceSendNotificationBean", mappedName = "TimerServiceSendNotificationBean")
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = REQUIRES_NEW)
public class TimerServiceSendNotificationBean implements
		TimerServiceSendNotification {

	@Resource
	TimerService timerService;
	static Logger logger = Logger.getRootLogger();
	String logId;

	public void createTimer(String timerName, long expiration, String logId) {
		this.logId = logId;
		logger.info("["+logId+"] createTimer() .... TimerService: "
				+ timerName + " expiration " + expiration);

		logger.debug("["+logId+"] \n\ttimerService [" + timerService + "]");

		Timer timer = timerService.createTimer(expiration, timerName);

	}

	@Timeout
	public void timeout(Timer timer) {
		logger.info("\n\tTimeout occurred !!!" + timer.getInfo());
		try {
			JMSMessageHandler jmsH = JMSMessageHandler.init();
			String[] timerInfo = timer.getInfo().toString().split("-");
			logger.debug("["+logId+"] timerInfo[3] jndi " + timerInfo[3]);
			logger.debug("["+logId+"] timerInfo[2] queue name" + timerInfo[2]);
			logger.debug("["+logId+"] timerInfo[1] messageText" + timerInfo[1]);
			jmsH.sendTextMessage(timerInfo[1], timerInfo[2], timerInfo[3]);
		} catch (MissingPropertyFileException e) {
			logger.fatal("["+logId+"]  "+e.getMessage());
			closeTimer(timer.getInfo().toString());
		}
	}

	public void closeTimer(String timerName) {
		Collection<Timer> timers = timerService.getTimers();
		for (Timer t : timers) {
			if (t.getInfo().equals(timerName)) {
				logger.debug("["+logId+"] closing timer:" + t.toString());
				t.cancel();
			}

		}
	}

	@Override
	public void createTimer(String timerName) {
		// TODO Auto-generated method stub

	}

}
