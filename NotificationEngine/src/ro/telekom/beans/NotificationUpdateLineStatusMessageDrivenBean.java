package ro.telekom.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;

import com.sun.mail.imap.protocol.UID;

import ro.telekom.exception.JDBCConnectionException;
import ro.telekom.exception.MissingPropertyFileException;
import ro.telekom.exception.NotificationProcessingException;
import ro.telekom.log.CustomLogger;
import ro.telekom.util.JDBCConnectionHandler;
import ro.telekom.util.JMSMessageHandler;
import ro.telekom.value.GenericProperties;
import ro.telekom.value.NotificationBatchResponse;
import ro.telekom.value.NotificationResponse;
import ro.telekom.value.SQLProperties;
import ro.telekom.value.enumerator.NotificationStatus;

@MessageDriven(messageListenerInterface = javax.jms.MessageListener.class, name = "NotificationUpdateLineStatusMessageDrivenBean", mappedName = "jms/queue/NotificationUpdateLineStatusJMSQueue", activationConfig = {
		@ActivationConfigProperty(propertyName = "connectionFactoryJndiName", propertyValue = "jms/cf/NotificationFilesConnectionFactory"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "workManager", propertyValue = "javax.jms.Queue")})
@TransactionManagement(TransactionManagementType.CONTAINER)
public class NotificationUpdateLineStatusMessageDrivenBean implements
		MessageListener, ExceptionListener {

	
	@Resource
	private MessageDrivenContext context;
	private Connection connection;
	static Logger logger = CustomLogger.getInstance();
	private int fileID;
	String logId;
	@Override
	public void onException(JMSException arg0) {

	}

	@Override
	public void onMessage(Message message) {
		try {
//			logger.info("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean I received a TextMessage from NotificationUpdateLineStatusJMSQueue");
			if (message instanceof ObjectMessage) {
				
				ObjectMessage om = (ObjectMessage) (message);
				NotificationBatchResponse batchResponse = (NotificationBatchResponse) om
						.getObject();
				setFileID(batchResponse.getFileId());
				logId = batchResponse.getProcessId();
				logger.info("["+logId+"] ["+logId+"] NotificationUpdateLineStatusMessageDrivenBean "
						+ "ObjectMessage received. ");
				if (batchResponse != null
						&& batchResponse.getNotificationResponses() != null
						&& batchResponse.getNotificationResponses().size() > 0) {
					initDB();
					updateDeviceResponseMessage(batchResponse);
					logger.info("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean updateDeviceResponseMessage "+getFileID()+"   END");
					int count = checkForFinished();
					logger.info("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean unprocessed yet: "
									+ count);
					if (count > 0) {
						logger.info("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean onMessage "+getFileID()+"   Finished.");
					} else {
						logger.info("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean "+getFileID()+"  onMessage Finished. A message will be send file for completion.");
						putMessageOnQueue("NOTIFICATION_FILE_REPORT_QUEUE_NAME");
					}
				}
			} else {
				logger.error("["+logId+"] Not valid message for NotificationUpdateLineStatusMessageDrivenBean");
			}
		} catch (Exception e) {
			logger.fatal("["+logId+"] " +e.getMessage());
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				logger.fatal("["+logId+"] " +e.getMessage());
			}
		}

	}

	/**
	 * Puts the message of fileId back so as the notification mechanism starts
	 * again in order continue with the rest records...
	 * 
	 * @throws MissingPropertyFileException
	 */
	private void putMessageOnQueue(String queueNameProperty)
			throws MissingPropertyFileException {
		JMSMessageHandler jms = JMSMessageHandler.init();
		jms.sendTextMessage(String.valueOf(getFileID())+"#<#"+logId, GenericProperties
				.getInstance().getProperty(queueNameProperty),
				GenericProperties.getInstance().getProperty("QCF_NAME"));
	}

	// private void updateDeviceStatus(NotificationBatchResponse batchResponse)
	// throws NotificationProcessingException {
	// logger.info("["+logId+"] [updateDeviceStatus] Starting...");
	// List<String> deviceIdsSucceed = new ArrayList<String>();
	// List<String> deviceIdsFailed = new ArrayList<String>();
	// List<NotificationResponse> responses = batchResponse
	// .getNotificationResponses();
	// List<String> batchDeviceIds = batchResponse.getDeviceIds();
	// logger.debug("["+logId+"] [updateDeviceStatus] batchResponsegetDeviceIds "
	// + batchResponse.getDeviceIds().size());
	// logger.debug("["+logId+"] [updateDeviceStatus] batchResponse getNotificationResponses "
	// + batchResponse.getNotificationResponses().size());
	//
	// for (NotificationResponse notificationResponse : responses) {
	// String currentDeviceId = notificationResponse.getKey();
	// if (batchDeviceIds.contains(currentDeviceId)) {
	// logger.debug("["+logId+"] key: " + notificationResponse.getKey()
	// + "\n getErrorMessage: "
	// + notificationResponse.getErrorMessage()
	// + "\n isSucceed: " + notificationResponse.isSucceed()
	// + "\n isInstalled:  "
	// + notificationResponse.isInstalled());
	// if (!notificationResponse.isSucceed()
	// || notificationResponse.getErrorMessage() != null) {
	// logger.debug("["+logId+"] key: " + notificationResponse.getKey()
	// + " added to failed.");
	// deviceIdsFailed.add(currentDeviceId);
	//
	// continue;
	// }
	// logger.debug("["+logId+"] key: " + notificationResponse.getKey()
	// + " added to success.");
	// deviceIdsSucceed.add(currentDeviceId);
	// } else {
	// logger.error("["+logId+"] [updateDeviceStatus] : " + getFileID()
	// + " - DeviceId : " + currentDeviceId
	// + " does not exist");
	// // @TODO Vale minima se oura oti den einai installed to app
	// }
	// }
	// logger.debug("["+logId+"] [updateDeviceStatus] deviceIdsSucceed : "
	// + deviceIdsSucceed);
	// logger.debug("["+logId+"] [updateDeviceStatus] deviceIdsFailed : "
	// + deviceIdsFailed);
	//
	// if (deviceIdsFailed != null && deviceIdsFailed.size() > 0) {
	// try {
	// updateLineStatus(deviceIdsFailed,
	// NotificationStatus.FAILED.getStatus());
	// } catch (SQLException e) {
	// throw new NotificationProcessingException(e.getMessage()
	// + "Cannot updateDeviceStatus FAILED : "
	// + getFileID(), false, e.getCause());
	// } catch (Exception ex) {
	// throw new NotificationProcessingException(ex.getMessage()
	// + "Cannot updateDeviceStatus FAILED : "
	// + getFileID(), false, ex.getCause());
	// }
	// }
	//
	// if (deviceIdsSucceed != null && deviceIdsSucceed.size() > 0) {
	// try {
	// updateLineStatus(deviceIdsSucceed,
	// NotificationStatus.COMPLETED.getStatus());
	// } catch (SQLException e) {
	// throw new NotificationProcessingException(e.getMessage()
	// + "[updateDeviceStatus]  " + getFileID(), false,
	// e.getCause());
	// } catch (Exception ex) {
	// throw new NotificationProcessingException(ex.getMessage()
	// + "[updateDeviceStatus] " + getFileID(), false,
	// ex.getCause());
	// }
	// }
	// logger.info("["+logId+"] [updateDeviceStatus] Ends...");
	// }

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private void initDB() throws NotificationProcessingException {
		if (logger.isDebugEnabled())
			logger.debug("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean initDB starts");
		try {
			JDBCConnectionHandler jdbc = new JDBCConnectionHandler();
			connection = jdbc.getDefaultDBConn();
			if (logger.isDebugEnabled())
				logger.debug("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean initDB ends");
		} catch (JDBCConnectionException e) {
			throw new NotificationProcessingException(e.getMessage()
					+ " initDB() FAILED : ", true, e.getCause());
		} catch (Exception ex) {
			throw new NotificationProcessingException(ex.getMessage()
					+ " initDB() FAILED: ", true, ex.getCause());
		}

	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private void closeDB(PreparedStatement prepStmt, ResultSet rs)
			throws NotificationProcessingException {
		try {
			if (prepStmt != null) {
				try {
					prepStmt.close();
				} catch (Exception e) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
			}
		} catch (Exception ex) {
			throw new NotificationProcessingException(ex.getMessage()
					+ " closeDB() FAILED: ", true, ex.getCause());
		}

	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private void updateDeviceResponseMessage(
			NotificationBatchResponse batchResponse)
			throws NotificationProcessingException, SQLException {
		try {
			logger.info("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean [updateDeviceResponseMessage] "+getFileID()+" starts.");
			List<NotificationResponse> responses = batchResponse
					.getNotificationResponses();
			List<String> batchDeviceIds = batchResponse.getDeviceIds();
			logger.debug("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean [updateDeviceResponseMessage] "+getFileID()+" batchResponsegetDeviceIds "
					+ batchResponse.getDeviceIds().size());
			logger.debug("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean [updateDeviceResponseMessage] "+getFileID()+" batchResponse getNotificationResponses "
					+ batchResponse.getNotificationResponses().size());
			for (NotificationResponse notificationResponse : responses) {
				String currentDeviceId = notificationResponse.getKey();
				if (batchDeviceIds.contains(currentDeviceId)) {
					logger.debug("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean [updateDeviceResponseMessage] "+getFileID()+" key: " + notificationResponse.getKey()
							+ "\n getErrorMessage: "
							+ notificationResponse.getErrorMessage()
							+ "\n isSucceed: "
							+ notificationResponse.isSucceed()
							+ "\n isInstalled:  "
							+ notificationResponse.isInstalled());
					if (!notificationResponse.isSucceed()
							|| notificationResponse.getErrorMessage() != null) {
						logger.debug("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean [updateDeviceResponseMessage] "+getFileID()+" key: " + notificationResponse.getKey()
								+ " added to failed.");
						updateLineDetails(notificationResponse.getKey(),
								NotificationStatus.FAILED.getStatus(),
								notificationResponse.isInstalled(),
								notificationResponse.getErrorMessage());
						continue;
					} else {
						logger.debug("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean [updateDeviceResponseMessage] "+getFileID()+" key: " + notificationResponse.getKey()
								+ " added to succeed.");
						updateLineDetails(notificationResponse.getKey(),
								NotificationStatus.COMPLETED.getStatus(),
								notificationResponse.isInstalled(),
								notificationResponse.getErrorMessage());
						continue;
					}

				} else {
					logger.error("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean [updateDeviceResponseMessage] : "
							+ getFileID() + " - DeviceId : " + currentDeviceId
							+ " does not exist");
				}
			}
			connection.commit();
		} catch (Exception e) {
			throw new NotificationProcessingException(e.getMessage()
					+ "[updateDeviceResponseMessage] FAILED  : " + getFileID(),
					true, e.getCause());
		}
		logger.info("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean [updateDeviceResponseMessage] "+getFileID()+" ends.");

	}

	// @TransactionAttribute(TransactionAttributeType.MANDATORY)
	// private void updateLineStatus(List<String> deviceIdsInput, int status)
	// throws SQLException, NotificationProcessingException {
	// PreparedStatement prepStmt=null;
	// ResultSet rs = null;
	// System.out.println("initDB");
	// initDB();
	// logger.info("["+logId+"] [updateLineStatus]  Starting... ");
	// String devicesIdsStr = "";
	// for (String deviceId : deviceIdsInput) {
	// devicesIdsStr += ",'" + deviceId + "'";
	// }
	// devicesIdsStr = devicesIdsStr.replaceFirst(",", "");
	// logger.debug("["+logId+"] devicesIdsStr: " + devicesIdsStr + " status : " + status);
	// try {
	// if (!devicesIdsStr.equals("")) {
	// String query = SQLProperties.getInstance().getProperty(
	// "UPDATE.FILE.LINES.BY.INSTALLATION.ID")
	// + devicesIdsStr + ")";
	// logger.debug("["+logId+"] [updateLineStatus] " + getFileID() + " query: "
	// + query);
	// prepStmt = connection.prepareStatement(query);
	// prepStmt.setInt(1, status);
	// prepStmt.executeUpdate();
	// connection.commit();
	// }
	// } catch (SQLException e) {
	// throw new NotificationProcessingException(e.getMessage()
	// + "[updateLineStatus] FAILED  : " + getFileID(), true,
	// e.getCause());
	// } catch (Exception ex) {
	// throw new NotificationProcessingException(ex.getMessage()
	// + "[updateLineStatus] FAILED  : " + getFileID(), true,
	// ex.getCause());
	// } finally {
	// System.out.println("closeDB");
	// closeDB(prepStmt, rs);
	// }
	//
	// }

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private void updateLineDetails(String installationId, int status,
			boolean isInstalled, String errorMessage)
			throws NotificationProcessingException {
		logger.info("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean [updateLineDetails] "+getFileID()+" starts. ");
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		try {
			String query = SQLProperties.getInstance().getProperty(
					"UPDATE.FILE.LINES.DETAILS");
			logger.debug("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean [updateLineDetails] " + getFileID() + " query: "
					+ query);
			prepStmt = connection.prepareStatement(query);
			// STATUS = ?, RESPONSE_MESSAGE = ?, IS_INSTALLED =? WHERE
			// INSTALLATION_ID = ?
			if(errorMessage!=null && errorMessage.length()>500)
				errorMessage = errorMessage.substring(0, 500);
			prepStmt.setString(4, installationId);
			prepStmt.setInt(1, status);
			prepStmt.setString(2,errorMessage);
			prepStmt.setBoolean(3, isInstalled);
			prepStmt.setInt(5, getFileID());
			prepStmt.executeUpdate();

		} catch (SQLException e) {
			throw new NotificationProcessingException(e.getMessage()
					+ "NotificationUpdateLineStatusMessageDrivenBean [updateLineStatus] FAILED  : " + getFileID(), true,
					e.getCause());
		} catch (Exception ex) {
			throw new NotificationProcessingException(ex.getMessage()
					+ "NotificationUpdateLineStatusMessageDrivenBean [updateLineStatus] FAILED  : " + getFileID(), true,
					ex.getCause());
		} finally {
			closeDB(prepStmt, rs);
		}

	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private int checkForFinished() throws NotificationProcessingException {
		PreparedStatement prepStmt = null;
		ResultSet rs = null;
		try {
			logger.info("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean [checkForFinished] " +getFileID() +" starts.");
			prepStmt = connection.prepareStatement(SQLProperties.getInstance()
					.getProperty("SELECT.UNPROCESSED.BY.FILE_ID"));
			prepStmt.setInt(1, getFileID());
			rs = prepStmt.executeQuery();
			if (rs != null && rs.next()) {
				logger.info("["+logId+"] NotificationUpdateLineStatusMessageDrivenBean [checkForFinished]: " + getFileID() + " false");
				return rs.getInt("count");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new NotificationProcessingException(e.getMessage()
					+ "Cannot checkForFinished fileId : " + getFileID(), true,
					e.getCause());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new NotificationProcessingException(ex.getMessage()
					+ "Cannot checkForFinished fileId : " + getFileID(), true,
					ex.getCause());
		} finally {
			try {
				closeDB(prepStmt, rs);
			} catch (NotificationProcessingException sqlex) {
				throw new NotificationProcessingException(sqlex.getMessage()
						+ "Cannot checkForFinished fileId : " + getFileID(),
						true, sqlex.getCause());
			}
		}
		return 0;

	}
	

	public int getFileID() {
		return fileID;
	}

	public void setFileID(int fileID) {
		this.fileID = fileID;
	}

}
