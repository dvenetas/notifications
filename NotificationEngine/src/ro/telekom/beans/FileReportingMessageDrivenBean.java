package ro.telekom.beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ro.telekom.exception.JDBCConnectionException;
import ro.telekom.exception.MissingPropertyFileException;
import ro.telekom.log.CustomLogger;
import ro.telekom.util.JDBCConnectionHandler;
import ro.telekom.value.SQLProperties;
import ro.telekom.value.enumerator.NotificationStatus;

@MessageDriven(messageListenerInterface = javax.jms.MessageListener.class, name = "FileReportingMessageDrivenBean", mappedName = "jms/queue/NotificationFilesReportJMSQueue", activationConfig = {
		@ActivationConfigProperty(propertyName = "connectionFactoryJndiName", propertyValue = "jms/cf/NotificationFilesConnectionFactory"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue") })
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FileReportingMessageDrivenBean implements MessageListener,
		ExceptionListener {

	Logger logger = CustomLogger.getInstance();
	String logId;
	@Resource
	private MessageDrivenContext context;

	@Override
	public void onException(JMSException arg0) {

	}

	@Override
	public void onMessage(Message message) {
		String status = "SUCCESS";
		String fileId = null;

		try {
			fileId = ((TextMessage) message).getText();
			String[] tokens = fileId.split("#<#");
			if (tokens.length > 1) {
				logId = tokens[1];
			} else {
				logId = UUID.randomUUID().toString();
				logger.info("Didn't find logId in input. Conitnue Logging with logId: ["
						+ logId + "]");
			}
			fileId = tokens[0];
			generateResponseFile(fileId);
		} catch (JMSException e) {
			logger.error(
					"["
							+ logId
							+ "]"
							+ "Failed to get the path from the JMS message. Exception: "
							+ e.getMessage(), e.getCause());
		} finally {
			logger.info("[" + logId + "]" + "File: " + fileId + " result: "
					+ status);
		}

	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private void generateResponseFile(String fileId) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		PrintWriter out = null;
		FileOutputStream fos = null;
		Connection con = null;
		try {
			con = (new JDBCConnectionHandler()).getDefaultDBConn();
			String getFilePathQuery = SQLProperties.getInstance().getProperty(
					"GET_FILE_INFO_BY_FILE_ID");
			stmt = con.prepareStatement(getFilePathQuery);
			stmt.setString(1, fileId);
			rs = stmt.executeQuery();
			String filePath = null, reportFilePath = null;
			while (rs.next()) {
				filePath = rs.getString(2);
				reportFilePath = filePath.replaceAll("process", "response");
			}
			String getLinesQuery = SQLProperties.getInstance().getProperty(
					"SELECT_LINES_BY_FILE_ID");
			stmt = con.prepareStatement(getLinesQuery);
			stmt.setString(1, fileId);
			rs = stmt.executeQuery();
			File reportFile = new File(reportFilePath);
			int fileLines = countFileLines(reportFile);
			fos = new FileOutputStream(reportFile);
			out = new PrintWriter(fos);
			int lineNr = 1;
			while (rs.next()) {
				// LINE NUMBER|MSISDN|INSTALLATION_ID|STATUS|LAST_UPDATE_DATE
				if (lineNr > fileLines) {
					out.println(lineNr
							+ "|"
							+ rs.getString("MSISDN")
							+ "|"
							+ rs.getString("INSTALLATION_ID")
							+ "|"
							+ NotificationStatus.translateNotificationStatus(rs
									.getInt("STATUS")) + "|"
							+ rs.getString("LAST_UPDATE_DATE"));
					logger.debug(lineNr + "|" + rs.getString("MSISDN") + "|"
							+ rs.getString("INSTALLATION_ID") + "|"
							+ rs.getInt("STATUS") + "|"
							+ rs.getString("LAST_UPDATE_DATE"));
				}
				lineNr++;
			}

			updateFileToComplete(Integer.parseInt(fileId), con);
			con.commit();

			File destDir = new File(filePath.replaceAll("process", "finish"));
			FileUtils.moveFileToDirectory(new File(filePath), destDir, true);
		} catch (SQLException e) {
			context.setRollbackOnly();
			logger.error("[" + logId + "]" + e.getMessage(), e.getCause());
		} catch (MissingPropertyFileException e) {
			logger.error("[" + logId + "]" + e.getMessage(), e.getCause());
		} catch (FileNotFoundException e) {
			logger.error("[" + logId + "]" + e.getMessage(), e.getCause());
		} catch (JDBCConnectionException e) {
			logger.error("[" + logId + "]" + e.getMessage(), e.getCause());
		} catch (NumberFormatException e) {
			logger.error("[" + logId + "]" + e.getMessage(), e.getCause());
		} catch (IOException e) {
			logger.error("[" + logId + "]" + e.getMessage(), e.getCause());
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					logger.fatal("[" + logId + "]" + "POSSIBLE CONNECTION LEAK");
				}
			}
			if (out != null) {
				out.close();
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	private void updateFileToComplete(int fileId, Connection con)
			throws SQLException, MissingPropertyFileException {
		logger.info("["
				+ logId
				+ "]"
				+ "[updateFileToComplete] Starting update to Completed for FileId: "
				+ fileId);
		PreparedStatement stmt = null;
		try {
			stmt = con.prepareStatement(SQLProperties.getInstance()
					.getProperty("UPATE.FILE.PARENT"));
			stmt.setInt(2, fileId);
			stmt.setInt(1, NotificationStatus.COMPLETED.getStatus());
			int success = stmt.executeUpdate();
			logger.info("[" + logId + "]" + "[updateFileToComplete] " + fileId
					+ " updated to COMPLETED. Response:" + success);
		} catch (SQLException e) {
			logger.error("[" + logId + "]" + e.getMessage(), e.getCause());
			throw e;
		} catch (MissingPropertyFileException e) {
			logger.error("[" + logId + "]" + e.getMessage(), e.getCause());
			throw e;
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("["
							+ logId
							+ "]"
							+ "[updateFileToComplete] - Failed to close Prepare Statement");
					e.printStackTrace();
				}
			}
		}
	}

	private int countFileLines(File f) {
		int numOfLines = 0;
		BufferedReader br = null;
		FileInputStream fis = null;
		try {
			if (f.exists()) {
				fis = new FileInputStream(f);
				br = new BufferedReader(new InputStreamReader(fis));
				while ((br.readLine()) != null) {
					numOfLines++;
				}
			}
		} catch (Exception e) {
			logger.error("[" + logId + "]" + e.getMessage(), e.getCause());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e) {
					logger.error(
							"[" + logId + "] - Failed to Close BufferedReader"
									+ e.getMessage(), e.getCause());
				}
			}
			if (fis != null) {

				try {
					fis.close();
				} catch (Exception e) {
					logger.error(
							"[" + logId + "] - Failed to Close FileInputStream"
									+ e.getMessage(), e.getCause());
				}
			}
		}
		return numOfLines;
	}

}
