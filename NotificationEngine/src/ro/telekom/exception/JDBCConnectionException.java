package ro.telekom.exception;

import java.io.Serializable;

public class JDBCConnectionException extends Exception implements Serializable {

	public JDBCConnectionException() {
	}

	public JDBCConnectionException(String pExceptionMsg) {
		super(pExceptionMsg);
	}

	public JDBCConnectionException(String pExceptionMsg, Throwable pException) {
		super(pExceptionMsg, pException);

	}

}
