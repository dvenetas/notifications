package ro.telekom.exception;

import java.io.Serializable;

public class NotificationProcessingException extends Exception implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7932315187557808843L;
	private boolean rollback = false;
	public NotificationProcessingException() {
	}

	public NotificationProcessingException(String pExceptionMsg, boolean rollback) {
		super(pExceptionMsg);
	}

	public NotificationProcessingException(String pExceptionMsg, boolean rollback, Throwable pException) {
		super(pExceptionMsg, pException);
		setRollback(rollback);
	}

	public boolean isRollback() {
		return rollback;
	}

	public void setRollback(boolean rollback) {
		this.rollback = rollback;
	}

}
