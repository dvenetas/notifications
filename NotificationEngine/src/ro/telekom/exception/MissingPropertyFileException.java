package ro.telekom.exception;

import java.io.Serializable;

public class MissingPropertyFileException extends Exception implements Serializable {

	public MissingPropertyFileException() {
	}

	public MissingPropertyFileException(String pExceptionMsg) {
		super(pExceptionMsg);
	}

	public MissingPropertyFileException(String pExceptionMsg, Throwable pException) {
		super(pExceptionMsg, pException);

	}

}
