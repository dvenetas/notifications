package ro.telekom.value.enumerator;

public enum PayloadType {
	SINGLE(1), MULTIPLE(2);

	private int type;

	private PayloadType(int c) {
		type = c;
	}

	public int getPayloadType() {
		return type;
	}
	
	public static String translatePayloadType(int c) {
		for (PayloadType type : PayloadType.values()) {
			if (type.getPayloadType() == c) {
				return type.toString();
			}
		}
		return "UNKNOWN";
	}
}
