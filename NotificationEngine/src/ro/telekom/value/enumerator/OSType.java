package ro.telekom.value.enumerator;

public enum OSType {
	 APPLE(1), ANDROID(2);
	 
	 private int osType;
	 
	 private OSType(int c) {
		 osType = c;
	 }
	 
	 public int getOSType() {
	   return osType;
	 }
}
