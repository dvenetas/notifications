package ro.telekom.value.enumerator;


public enum NotificationStatus {
	 INSERTED(1), FAILED(2), COMPLETED(3), UNKOWN(4);
	 
	 private int status;
	 
	 private NotificationStatus(int c) {
		 status = c;
	 }
	 
	 public int getStatus() {
	   return status;
	 }
	 
	public static String translateNotificationStatus(int c) {
		for (NotificationStatus status : NotificationStatus.values()) {
			if (status.getStatus() == c) {
				return status.toString();
			}
		}
		return "UNKNOWN";
	}
}
