package ro.telekom.value;

import java.io.Serializable;

public class NotificationResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8363955520091107914L;
	private String key;
	private boolean succeed;
	private String errorMessage;
	private boolean installed = true;
	private Long id;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public boolean isSucceed() {
		return succeed;
	}

	public void setSucceed(boolean succeed) {
		this.succeed = succeed;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public boolean isInstalled() {
		return installed;
	}

	public void setInstalled(boolean installed) {
		this.installed = installed;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
