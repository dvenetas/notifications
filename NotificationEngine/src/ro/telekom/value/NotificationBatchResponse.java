package ro.telekom.value;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NotificationBatchResponse implements Serializable {
	
	private static final long serialVersionUID = -6869294552922489960L;
	private List<NotificationResponse> notificationResponses= new ArrayList<NotificationResponse>();
	private List<String> deviceIds = new ArrayList<String>();
	private int fileId;
	private String processId;
	public List<NotificationResponse> getNotificationResponses() {
		return notificationResponses;
	}
	public void setNotificationResponses(List<NotificationResponse> notificationResponses) {
		this.notificationResponses = notificationResponses;
	}
	public List<String> getDeviceIds() {
		return deviceIds;
	}
	public void setDeviceIds(List<String> deviceIds) {
		this.deviceIds = deviceIds;
	}
	public int getFileId() {
		return fileId;
	}
	public void setFileId(int fileId) {
		this.fileId = fileId;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
}
