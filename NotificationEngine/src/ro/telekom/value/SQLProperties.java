package ro.telekom.value;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import ro.telekom.exception.MissingPropertyFileException;

public class SQLProperties extends Properties {

	private static Properties propCache = null;

	private SQLProperties() {

	}

	private static void initProperties() throws MissingPropertyFileException {
		String propertyFileName = System.getProperty("user.dir")
				+ System.getProperty("file.separator") + "propertyFiles"
				+ System.getProperty("file.separator") + "sql.properties";
		FileInputStream in;
		try {
			in = new FileInputStream(propertyFileName);

			propCache = new Properties();
			propCache.load(in);
			in.close();
			Enumeration<Object> e = propCache.keys();
		} catch (FileNotFoundException e) {
			throw new MissingPropertyFileException(propertyFileName);
		} catch (IOException e1) {
			throw new MissingPropertyFileException(propertyFileName);
		}

	}

	public static Properties getInstance() throws MissingPropertyFileException {
		if (propCache == null) {
			propCache = new Properties();
			initProperties();
		}
		return propCache;
	}

	public static void refresh() throws MissingPropertyFileException {
		propCache = new Properties();
		initProperties();
	}

}
