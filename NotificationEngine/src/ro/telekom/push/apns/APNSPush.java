package ro.telekom.push.apns;

//Object devices: a list of device tokens to push your notification
// to. Note that you should always pass a list of device tokens if
// you have one instead of creating a loop and invoking a method
// repetitively with each token. This is because Apple strongly
// encourages users to send multiple notifications on a single
// connection, instead of opening and closing connections for each
// notification. Passing your list of device tokens directly to
// JavaPNS will allow the library to optimize the connection usage.
// You can pass the following objects to this parameter:
//       Status Codes & Meanings
//       0  - No errors encountered
//       1   - Processing error
//       2   - Missing device token
//       3   - Missing topic
//       4   - Missing payload
//       5   - Invalid token size
//       6   - Invalid topic size
//       7   - Invalid payload size
//       8   - Invalid token
//       255 - None (unknown)
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javapns.Push;
import javapns.devices.Device;
import javapns.devices.exceptions.InvalidDeviceTokenFormatException;
import javapns.devices.implementations.basic.BasicDevice;
import javapns.notification.PayloadPerDevice;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;

import org.apache.log4j.Logger;
import org.json.JSONException;

import ro.telekom.exception.MissingPropertyFileException;
import ro.telekom.log.CustomLogger;
import ro.telekom.value.GenericProperties;
import ro.telekom.value.NotificationResponse;

public class APNSPush {

	private boolean production;
	// static Logger logger = CustomLogger.getInstance();
	Logger logger = CustomLogger.getInstance();
	String logId;

	public APNSPush(String fileName, String processId) {
		this.logId = processId;
	}

	public List<NotificationResponse> sendPush(String message,
			List<String> deviceIds) throws MissingPropertyFileException {
		List<NotificationResponse> notificationResponses = new LinkedList<NotificationResponse>();
		boolean failed = false;
		String errorMessage = "";
		try {
			logger.info("[" + logId + "] APNS : [sendPush] starts.");
			setProduction(Boolean.valueOf(GenericProperties.getInstance()
					.getProperty("isProduction")));
			logger.debug("[" + logId + "] APNS : [sendPush] isPRODUCTION "
					+ isProduction());
			if (message != null && !message.equalsIgnoreCase("")) {
				// /* Build a blank payload to customize */
				PushNotificationPayload payload = PushNotificationPayload
						.complex();
				/* Customize the payload */
				payload.addAlert(message);

				Object keystore = GenericProperties.getInstance().getProperty(
						"APNS_CERT_PATH");
				;// @TODO fill
				String password = GenericProperties.getInstance().getProperty(
						"APNS_PASS");// @TODO fill
				logger.debug("[" + logId
						+ "] APNS : [sendPushPerDevice] keystore " + keystore);
				logger.debug("[" + logId
						+ "] APNS : [sendPushPerDevice] password " + password);
				int threads = 3;// @TODO discuss it

				List<Device> devices = new ArrayList<Device>();
				for (String deviceId : deviceIds) {
					logger.debug("["
							+ logId
							+ "] APNS : [sendPush] PREPARE input: add to device deviceId for apple: "
							+ deviceId);
					try {
						devices.add(new BasicDevice(deviceId));
					} catch (InvalidDeviceTokenFormatException e) {
						logger.error("[" + logId
								+ "] InvalidDeviceTokenFormatException "
								+ e.getMessage());
						NotificationResponse notificationResponse = new NotificationResponse();
						notificationResponse.setKey(deviceId);
						notificationResponse
								.setErrorMessage("DeviceId is invalid.");
						notificationResponse.setInstalled(false);
						notificationResponse.setSucceed(false);
						notificationResponses.add(notificationResponse);
					}
				}
				if (devices.size() > 0) {
					Calendar cal = Calendar.getInstance();

					logger.debug("[" + logId + "] payload " + payload);
					logger.debug("[" + logId + "] keystore " + keystore);
					logger.debug("[" + logId + "] password " + password);
					logger.debug("[" + logId + "] isProduction "
							+ isProduction());
					logger.debug("[" + logId + "] threads " + threads);
					logger.debug("[" + logId + "] devices size "
							+ devices.size());
					List<PushedNotification> notifications_thread = Push
							.payload(payload, keystore, password,
									isProduction(), threads, devices);
					logger.debug("[" + logId
							+ "] Starting processing responses");
					if (notifications_thread == null
							|| notifications_thread.size() == 0) {
						failed = true;
						errorMessage = "APNS Push.payload returned empty response";
					}
					for (PushedNotification npt : notifications_thread) {
						logger.debug("[" + logId + "] "
								+ npt.getDevice().getDeviceId()
								+ ": isSuccessful " + npt.isSuccessful());
						NotificationResponse notificationResponse = new NotificationResponse();
						notificationResponse.setKey(npt.getDevice()
								.getDeviceId());
						notificationResponse.setSucceed(npt.isSuccessful());

						if (npt.getResponse() != null
								&& npt.getResponse().getMessage() != null)
							logger.debug("[" + logId + "] "
									+ npt.getDevice().getDeviceId()
									+ ": getResponse "
									+ npt.getResponse().getMessage());
						if (npt.getException() != null
								&& npt.getException().getMessage() != null) {
							logger.debug("[" + logId + "] "
									+ npt.getDevice().getDeviceId()
									+ ": getException "
									+ npt.getException().getMessage());
							notificationResponse.setErrorMessage(npt
									.getException().getMessage());
							if(npt.getException() instanceof InvalidDeviceTokenFormatException){
								notificationResponse.setInstalled(false);
							}
							if(npt.getException().getMessage().indexOf("Invalid token")>-1){
								notificationResponse.setInstalled(false);
							}
							
						}

						notificationResponses.add(notificationResponse);
					}
				} else {
					failed = true;
					errorMessage = "DeviceIds are Invalid/Empty.";
				}
			} else {
				failed = true;
				errorMessage = "Notification Message is empty. Please check!";
			}

		} catch (JSONException ex) {
			ex.printStackTrace();
			failed = true;
			errorMessage = ex.getMessage();
		} catch (Exception e) {
			failed = true;
			errorMessage = e.getMessage();
		}
		if (failed) {
			logger.error("[" + logId + "] APNSPush.sendPush Failed "
					+ errorMessage);
			logger.error("["
					+ logId
					+ "] APNSPush.sendPush Failed. The whole batch will be failed.");
			for (int i = 0; i < deviceIds.size(); i++) {
				NotificationResponse notificationResponse = new NotificationResponse();
				notificationResponse.setKey(deviceIds.get(i));
				notificationResponse.setErrorMessage(errorMessage);
				if (errorMessage != null
						&& errorMessage
								.equalsIgnoreCase("DeviceIds are Invalid/Empty.")) {
					notificationResponse.setInstalled(false);
				}
				notificationResponse.setSucceed(false);
				notificationResponses.add(notificationResponse);
			}
		}
		logger.info("[" + logId + "] APNS : [sendPush] ends.");
		return notificationResponses;
	}

	public List<NotificationResponse> sendPushPerDevice(
			HashMap<String, String> messageDeviceMap) {
		List<NotificationResponse> notificationResponses = new LinkedList<NotificationResponse>();
		boolean failed = false;
		String errorMessage = "";
		try {
			logger.info("[" + logId + "] APNS : [sendPush] starts.");
			setProduction(Boolean.valueOf(GenericProperties.getInstance()
					.getProperty("isProduction")));
			logger.debug("[" + logId
					+ "] APNS : [sendPushPerDevice] isPRODUCTION "
					+ isProduction());
			List<PayloadPerDevice> ppdList = new LinkedList<PayloadPerDevice>();
			for (Map.Entry<String, String> entry : messageDeviceMap.entrySet()) {
				logger.debug("["
						+ logId
						+ "] APNS : [sendPushPerDevice] prepare input entry.getKey(): "
						+ entry.getKey() + " and entry.getValue(): "
						+ entry.getValue());
				String key = entry.getKey();
				String message = entry.getValue();
				if (message != null && !message.equalsIgnoreCase("")) {
					PushNotificationPayload payload = PushNotificationPayload
							.alert(message);
					try {
						ppdList.add(new PayloadPerDevice(payload, key));
					} catch (InvalidDeviceTokenFormatException e) {
						NotificationResponse notificationResponse = new NotificationResponse();
						notificationResponse.setKey(entry.getKey());
						notificationResponse
								.setErrorMessage("InvalidDeviceTokenFormatException. Please check");
						notificationResponse.setSucceed(false);
						notificationResponses.add(notificationResponse);
					} catch (Exception e) {
						NotificationResponse notificationResponse = new NotificationResponse();
						notificationResponse.setKey(entry.getKey());
						notificationResponse.setErrorMessage(e.getMessage());
						notificationResponse.setSucceed(false);
						notificationResponses.add(notificationResponse);
					}
				} else {
					NotificationResponse notificationResponse = new NotificationResponse();
					notificationResponse.setKey(entry.getKey());
					notificationResponse
							.setErrorMessage("Notification Message is empty. Please check");
					notificationResponse.setSucceed(false);
					notificationResponses.add(notificationResponse);
				}

			}

			Object keystore = GenericProperties.getInstance().getProperty(
					"APNS_CERT_PATH");
			;// @TODO fill
			String password = GenericProperties.getInstance().getProperty(
					"APNS_PASS");// @TODO fill
			logger.debug("[" + logId + "] APNS : [sendPushPerDevice] keystore "
					+ keystore);
			logger.debug("[" + logId + "] APNS : [sendPushPerDevice] password "
					+ password);
			List<PushedNotification> notifications_thread = Push.payloads(
					keystore, password, isProduction(), ppdList);

			for (PushedNotification npt : notifications_thread) {
				logger.debug("[" + logId + "] "
						+ npt.getDevice().getDeviceId()
						+ ": isSuccessful " + npt.isSuccessful());
				NotificationResponse notificationResponse = new NotificationResponse();
				notificationResponse.setKey(npt.getDevice()
						.getDeviceId());
				notificationResponse.setSucceed(npt.isSuccessful());

				if (npt.getResponse() != null
						&& npt.getResponse().getMessage() != null)
					logger.debug("[" + logId + "] "
							+ npt.getDevice().getDeviceId()
							+ ": getResponse "
							+ npt.getResponse().getMessage());
				if (npt.getException() != null
						&& npt.getException().getMessage() != null) {
					logger.debug("[" + logId + "] "
							+ npt.getDevice().getDeviceId()
							+ ": getException "
							+ npt.getException().getMessage());
					notificationResponse.setErrorMessage(npt
							.getException().getMessage());
					if(npt.getException() instanceof InvalidDeviceTokenFormatException)
						notificationResponse.setInstalled(false);
					
					if(npt.getException().getMessage().indexOf("Invalid token")>-1)
						notificationResponse.setInstalled(false);
					
				}

				notificationResponses.add(notificationResponse);
			}
			
		} catch (Exception e) {
			logger.error("[" + logId + "] APNSPush.sendPushPerDevice Failed "
					+ e.getMessage());
			logger.error("["
					+ logId
					+ "] APNSPush.sendPushPerDevice Failed. The whole batch will be failed.");
			failed = true;
			errorMessage = e.getMessage();

		}
		if (failed) {
			for (Map.Entry<String, String> entry : messageDeviceMap.entrySet()) {
				logger.debug("["
						+ logId
						+ "] APNS : [sendPushPerDevice] FAILED entry.getKey(): "
						+ entry.getKey() + " and entry.getValue(): "
						+ entry.getValue());
				NotificationResponse notificationResponse = new NotificationResponse();
				notificationResponse.setKey(entry.getKey());
				notificationResponse.setErrorMessage(errorMessage);
				notificationResponse.setSucceed(false);
				notificationResponses.add(notificationResponse);
			}
		}
		logger.info("[" + logId + "] APNS : [sendPush] ends.");
		return notificationResponses;
	}

	public boolean isProduction() {
		return production;
	}

	public void setProduction(boolean production) {
		this.production = production;
	}

	public static void main(String[] args) {
		try {
//			byte[] dsdfs ="dfsdfsfs dfdsfds fdsfdsf fsdfsfdsd dfsdfsfs dfdsfds fdsfdsf fsdfsfdsd  dfsdfsfs dfdsfds fdsfdsf fsdfsfdsd".get ;
//			System.out.println(dsdfs.length);
//			SmackCcsClient ccsClient = new SmackCcsClient();
//
//			try {
//				ccsClient.connect(userName, GOOGLE_SERVER_KEY);
//			} catch (XMPPException e) {
//				e.printStackTrace();
//			}
//
//			String messageId = ccsClient.getRandomMessageId();
//			Map<String, String> payload = new HashMap<String, String>();
//			payload.put(MESSAGE_KEY, message);
//			payload.put("EmbeddedMessageId", messageId);
//			String collapseKey = "sample";
//			Long timeToLive = 10000L;
//			Boolean delayWhileIdle = true;
//			ccsClient.send(createJsonMessage(toDeviceRegId, messageId, payload,
//					collapseKey, timeToLive, delayWhileIdle));
			
			
			
//			List<Device> devices = new ArrayList<Device>();
//			devices.add(new BasicDevice(
//					"e685a3321cd0309358343dd752836caddd2ddcbdbe2185fad02a660fec86e349"));
//			PushNotificationPayload payload = PushNotificationPayload.complex();
//			/* Customize the payload */
//			payload.addAlert("Nasia lala");
//			System.out.println("1");
//			List<PushedNotification> notifications_thread = Push
//					.payload(
//							payload,
//							"C:/dev/Cosmote_ro/wl/pr/d/cognity/propertyFiles/apns_dev.p12/",
//							"cognity", false, 1, devices);
//
//			System.out.println("2");
//
//			for (PushedNotification npt : notifications_thread) {
//				if(npt.getException() instanceof InvalidDeviceTokenFormatException){
////					npt.is
//				}
////				System.out.println(npt.getException().equals());
//				System.out.println(npt.getDevice().getDeviceId()
//						+ ": isSuccessful " + npt.isSuccessful());
//				if (npt.getResponse() != null
//						&& npt.getResponse().getMessage() != null) {
//					System.out
//							.println(": getResponse " + npt.getResponse() != null ? npt
//									.getResponse().getMessage() : "lala");
//				}
//				System.out.println(": getResponse " + npt.getException());
//				if (npt.getResponse() != null
//						&& npt.getResponse().getMessage() != null) {
//					System.out
//							.println(": getResponse " + npt.getResponse() != null ? npt
//									.getResponse().getMessage() : "lala");
//				}
//				if (npt.getResponse() != null && npt.getException() != null) {
//					System.out
//							.println(": getResponse " + npt.getResponse() != null ? npt
//									.getResponse().getMessage() : "lala");
//				}
//			}
			// char[] password = "password".toCharArray();
			// String alias = "alias";
			//
			// FileInputStream fIn = new FileInputStream(keystoreFilename);
			// KeyStore keystore = KeyStore.getInstance("JKS");
			//
			// keystore.load(fIn, password);
			//
			// Certificate cert = keystore.getCertificate(alias);
			//
			// System.out.println(cert);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
