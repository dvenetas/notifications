package ro.telekom.push.gcm;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import ro.telekom.exception.MissingPropertyFileException;
import ro.telekom.log.CustomLogger;
import ro.telekom.value.GenericProperties;
import ro.telekom.value.NotificationResponse;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

public class GCMPush {
	Logger logger = CustomLogger.getInstance();
	private String collapseKey = "";
	String logId;

	public GCMPush(String fileName, String logId) {
		try {
			setCollapseKey(GenericProperties.getInstance().getProperty(
					"GCM_COLLAPSE_KEY"));
			this.logId = logId;
		} catch (MissingPropertyFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// // Put your Google Project number here
	// final String GOOGLE_USERNAME = "512212818580" + "@gcm.googleapis.com";
	// // Put your Google API Server Key here
	// private static final String GOOGLE_SERVER_KEY =
	// "AIzaSyA9DQTcggUtABVC9lnV_Xb5VEQ8iKBEaP4";
	// static final String MESSAGE_KEY = "SERVER_MESSAGE";
	// public void sendPushCCS(String userName,
	// final String GOOGLE_SERVER_KEY, String toDeviceRegId, String message) {
	//
	// SmackCcsClient ccsClient = new SmackCcsClient();
	//
	// try {
	// ccsClient.connect(userName, GOOGLE_SERVER_KEY);
	// } catch (XMPPException e) {
	// e.printStackTrace();
	// }
	//
	// String messageId = ccsClient.getRandomMessageId();
	// Map<String, String> payload = new HashMap<String, String>();
	// payload.put(MESSAGE_KEY, message);
	// payload.put("EmbeddedMessageId", messageId);
	// String collapseKey = "sample";
	// Long timeToLive = 10000L;
	// Boolean delayWhileIdle = true;
	// ccsClient.send(createJsonMessage(toDeviceRegId, messageId, payload,
	// collapseKey, timeToLive, delayWhileIdle));
	// }
	//
	// public static String createJsonMessage(String to, String messageId,
	// Map<String, String> payload, String collapseKey, Long timeToLive,
	// Boolean delayWhileIdle) {
	// Map<String, Object> message = new HashMap<String, Object>();
	// message.put("to", to);
	// if (collapseKey != null) {
	// message.put("collapse_key", collapseKey);
	// }
	// if (timeToLive != null) {
	// message.put("time_to_live", timeToLive);
	// }
	// if (delayWhileIdle != null && delayWhileIdle) {
	// message.put("delay_while_idle", true);
	// }
	// message.put("message_id", messageId);
	// message.put("data", payload);
	// return JSONValue.toJSONString(message);
	// }

	public List<NotificationResponse> sendPush(String notificationMessage,
			List<String> deviceIds) {
		logger.info("[" + logId + "] GCMPush [sendPush] starts.");
		List<NotificationResponse> notificationResponses = new LinkedList<NotificationResponse>();
		// No, the SENDER_ID is the project ID you signed up at Google API
		// console, it should a numeric string. e.g. on your browser URI, you
		// should see this:
		// https://code.google.com/apis/console/#project:4815162342
		//
		// The Sender ID is 4815162342
		Sender sender;
		boolean failed = false;
		String errorMessage = "";
		try {
			sender = new Sender(GenericProperties.getInstance().getProperty(
					"GCM_SENDER_ID"));
		} catch (MissingPropertyFileException e1) {
			e1.printStackTrace();
			return notificationResponses;
		}
		
		
		//
		// This Message object will hold the data that is being transmitted
		// to the Android client devices. For this demo, it is a simple text
		// string, but could certainly be a JSON object.
		if (notificationMessage != null
				&& !notificationMessage.equalsIgnoreCase("")) {
			Message message = new Message.Builder()
					.collapseKey(getCollapseKey()).timeToLive(3)
					.delayWhileIdle(true)
					.addData("message", notificationMessage).build();

			// If multiple messages are sent using the same .collapseKey()
			// the android target device, if it was offline during earlier
			// message
			// transmissions, will only receive the latest message for that key
			// when
			// it goes back on-line.
			// deviceIds.add("APA91bEXbIoKesiKk2BY4RdD8D0xA7uiatvxsXgPNS1oKh32F4OhYbq5ZFQwQDTBRaCVwVGbi8hMdwZeGN3SF8ZBRH58h3JU3cudRcrC0hl8BIl9W9MHf1Fa6-Z-fmn-a2jlysMC5YoUPGGhxotDZ2sncsDu-Dqrpw");
			try {
				// use this for multicast messages. The second parameter
				// of sender.send() will need to be an array of register ids.
				MulticastResult result = sender.send(message, deviceIds, 1);
				if (result.getResults() != null) {
					for (int i = 0; i < deviceIds.size(); i++) {
						logger.debug("[" + logId + "] deviceId) "
								+ deviceIds.get(i));
						logger.debug("[" + logId + "] " + deviceIds.get(i)
								+ ": getErrorCodeName "
								+ result.getResults().get(i).getErrorCodeName());
						logger.debug("[" + logId + "] " + deviceIds.get(i)
								+ ": getMessageId "
								+ result.getResults().get(i).getMessageId());
						logger.debug("[" + logId + "] " + deviceIds.get(i)
								+ ": getSuccess " + result.getSuccess());
						logger.debug("[" + logId + "] " + deviceIds.get(i)
								+ ": getFailure " + result.getFailure());
						logger.debug("[" + logId + "] " + deviceIds.get(i)
								+ ": getMulticastId " + result.getMulticastId());
						logger.debug("[" + logId + "] " + deviceIds.get(i)
								+ ": getRetryMulticastIds "
								+ result.getRetryMulticastIds());
						NotificationResponse notificationResponse = new NotificationResponse();
						notificationResponse.setKey(deviceIds.get(i));
						notificationResponse.setErrorMessage(result
								.getResults().get(i).getErrorCodeName());
						notificationResponse.setSucceed(true);
						if (notificationResponse.getErrorMessage() != null
								&& (notificationResponse.getErrorMessage()
										.equalsIgnoreCase("NotRegistered") || notificationResponse
										.getErrorMessage().equalsIgnoreCase(
												"InvalidRegistration")))
							notificationResponse.setInstalled(false);
						notificationResponses.add(notificationResponse);
					}
				}

			} catch (Exception e) {
				logger.error("[" + logId + "] GCMPush.sendPush Failed "
						+ e.getMessage());
				logger.error("["
						+ logId
						+ "] GCMPush.sendPush Failed. The whole batch will be failed.");
				failed = true;
				errorMessage = e.getMessage();
			}
		} else {
			failed = true;
			errorMessage = "Notification Message is empty. Please check!";

		}
		if (failed) {
			for (int i = 0; i < deviceIds.size(); i++) {
				NotificationResponse notificationResponse = new NotificationResponse();
				notificationResponse.setKey(deviceIds.get(i));
				notificationResponse.setErrorMessage(errorMessage);
				notificationResponse.setSucceed(false);
				notificationResponses.add(notificationResponse);
			}
		}
		logger.info("[" + logId + "] GCMPush [sendPush] ends.");
		return notificationResponses;
	}

	public List<NotificationResponse> sendPushPerDevice(
			HashMap<String, String> messageDeviceMap) {
		List<NotificationResponse> notificationResponses = new LinkedList<NotificationResponse>();
		logger.info("[" + logId + "] GCMPush [sendPushPerDevice] starts.");
		logger.debug("[" + logId
				+ "] GCMPush [sendPushPerDevice] messageDeviceMap : "
				+ messageDeviceMap.size());

		Sender sender;

		try {
			sender = new Sender(GenericProperties.getInstance().getProperty(
					"GCM_SENDER_ID"));
		} catch (MissingPropertyFileException e1) {
			e1.printStackTrace();
			return notificationResponses;
		}
		for (Map.Entry<String, String> entry : messageDeviceMap.entrySet()) {
			logger.debug("["
					+ logId
					+ "] GCM : [sendPushPerDevice] prepare input entry.getKey(): "
					+ entry.getKey() + " and entry.getValue(): "
					+ entry.getValue());
			String key = entry.getKey();
			String notificationMessage = entry.getValue();
			Message message = new Message.Builder()
					.collapseKey(getCollapseKey()).timeToLive(3)
					.delayWhileIdle(true)
					.addData("message", notificationMessage).build();
			Result result;
			try {
				NotificationResponse notificationResponse = new NotificationResponse();
				notificationResponse.setKey(key);
				if (entry.getValue() != null
						&& !entry.getValue().equalsIgnoreCase("")) {
					result = sender.send(message, key, 1);
					if (result != null) {
						logger.debug("[" + logId + "] key " + key);
						logger.debug("[" + logId + "] " + key
								+ ": getErrorCodeName "
								+ result.getErrorCodeName());
						logger.debug("[" + logId + "] " + key
								+ ": getMessageId " + result.getMessageId());

						notificationResponse.setKey(key);
						notificationResponse.setErrorMessage(result
								.getErrorCodeName());
						notificationResponse.setSucceed(true);
						if (result.getErrorCodeName() != null
								&& (result.getErrorCodeName().equalsIgnoreCase(
										"NotRegistered") || notificationResponse
										.getErrorMessage().equalsIgnoreCase(
												"InvalidRegistration"))) {
							notificationResponse.setInstalled(false);
						}
						notificationResponses.add(notificationResponse);

					} else {
						notificationResponse
								.setErrorMessage("Notification Message is empty. Please check!");
						notificationResponse.setSucceed(false);
					}
				}

			} catch (IOException e) {
				logger.error("[" + logId + "] GCMPush.sendPush Failed "
						+ e.getMessage());
				logger.error("["
						+ logId
						+ "] GCMPush.sendPush Failed. The whole batch will be failed.");
				NotificationResponse notificationResponse = new NotificationResponse();
				notificationResponse.setKey(key);
				notificationResponse.setErrorMessage(e.getMessage());
				notificationResponse.setSucceed(false);
				notificationResponses.add(notificationResponse);
			}

		}
		logger.info("[" + logId + "] GCMPush [sendPushPerDevice] ends.");
		return notificationResponses;
	}

	public String getCollapseKey() {
		return collapseKey;
	}

	public void setCollapseKey(String collapseKey) {
		this.collapseKey = collapseKey;
	}

}
