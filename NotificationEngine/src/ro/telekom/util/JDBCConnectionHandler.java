package ro.telekom.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import ro.telekom.exception.JDBCConnectionException;
import ro.telekom.exception.MissingPropertyFileException;
import ro.telekom.value.GenericProperties;

public class JDBCConnectionHandler {

	private static DataSource ds;

	public java.sql.Connection getDefaultDBConn()
			throws MissingPropertyFileException, JDBCConnectionException {

		Properties props = GenericProperties.getInstance();
		try {
			Connection c = getDBConn(props.getProperty("DB_DATA_SOURCE"),
					props.getProperty("DB_URL"),
					props.getProperty("DB_USERNAME"),
					props.getProperty("DB_PASSWORD"),
					props.getProperty("DB_SECURITY"));
			return c;
		} catch (SQLException e) {
			throw new JDBCConnectionException(e.getMessage(), e.getCause());
		} catch (NamingException e) {
			throw new JDBCConnectionException(e.getMessage(), e.getCause());
		}
	}

	public Connection getDBConn(String dataSourceName, String url,
			String username, String password, String securityPrincipal)
			throws SQLException, NamingException {

		Connection conn = null;
		Context ctx = null;

		try {
			DataSource newDataSource;
			if (ds == null) {
				ctx = getInitialContext(url, username, password,
						securityPrincipal);
				newDataSource = (DataSource) ctx.lookup(dataSourceName);
				ctx.close();
			} else {
				newDataSource = ds;
			}
			conn = newDataSource.getConnection();
			conn.setAutoCommit(false);
			return conn;
		} catch (SQLException se) {

			se.printStackTrace();
			throw se;

		} catch (NamingException ne) {

			ne.printStackTrace();
			try {
				ctx.close();
			} catch (NamingException e) {
				e.printStackTrace();
			}
			throw ne;

		}

	}

	static protected Context getInitialContext(String weblogic_url,
			String username, String password, String localBool)
			throws NamingException {
		Properties containerEnv = new Properties();
		containerEnv.setProperty(Context.INITIAL_CONTEXT_FACTORY,
				"weblogic.jndi.WLInitialContextFactory");
		containerEnv.setProperty(Context.PROVIDER_URL, weblogic_url);

		containerEnv.setProperty(Context.SECURITY_AUTHENTICATION, "simple");
		containerEnv.setProperty(Context.SECURITY_PRINCIPAL, username);
		containerEnv.setProperty(Context.SECURITY_CREDENTIALS, password);
		Context ctx = new InitialContext(containerEnv);
		return ctx;

	}
}
