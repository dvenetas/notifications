package ro.telekom.util;

import java.util.Hashtable;
import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.naming.Context;
import javax.naming.InitialContext;

import ro.telekom.exception.MissingPropertyFileException;
import ro.telekom.value.GenericProperties;
import ro.telekom.value.NotificationBatchResponse;

public class JMSNotificationResponseMessageHandler {
	private static JMSNotificationResponseMessageHandler jmsMessageHandler;
	private InitialContext ctx = null;
	private QueueConnectionFactory qcf = null;
	private QueueConnection qc = null;
	private QueueSession qsess = null;
	private Queue q = null;
	private QueueSender qsndr = null;
	private ObjectMessage message = null;

	public JMSNotificationResponseMessageHandler() {
		super();
	}

	public static JMSNotificationResponseMessageHandler init(){
		if(jmsMessageHandler == null){
			jmsMessageHandler = new JMSNotificationResponseMessageHandler();
		}
		return jmsMessageHandler;
	}

	public void sendNotificationResponseMessage(NotificationBatchResponse messageObject, String queueJNDI, String cfJNDI) throws MissingPropertyFileException {
		Properties p = GenericProperties.getInstance();
		String QCF_NAME = cfJNDI;
		String QUEUE_NAME = queueJNDI;
		String URL = p.getProperty("URL");
		String USER = p.getProperty("USER");
		String PASSWORD = p.getProperty("PASSWORD");
		sendNotificationResponseMessage(URL, USER, PASSWORD,
				QCF_NAME, QUEUE_NAME, messageObject);
	}

	public void sendNotificationResponseMessage(String url, String user, String password,
			String cf, String queue, NotificationBatchResponse messageObject) {
		// create InitialContext
		Hashtable<String,String> properties = new Hashtable<String, String>();
		properties.put(Context.INITIAL_CONTEXT_FACTORY,
				"weblogic.jndi.WLInitialContextFactory");
		properties.put(Context.PROVIDER_URL, url);
		properties.put(Context.SECURITY_PRINCIPAL, user);
		properties.put(Context.SECURITY_CREDENTIALS, password);

		try {
			ctx = new InitialContext(properties);
			qcf = (QueueConnectionFactory) ctx.lookup(cf);
			qc = qcf.createQueueConnection();
			qsess = qc.createQueueSession(false, 0);
			q = (Queue) ctx.lookup(queue);
			qsndr = qsess.createSender(q);
			message = qsess.createObjectMessage();
			message.setObject(messageObject);
			qsndr.send(message);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		// clean up
		try {
			message = null;
			qsndr.close();
			qsndr = null;
			q = null;
			qsess.close();
			qsess = null;
			qc.close();
			qc = null;
			qcf = null;
			ctx = null;
		} catch (JMSException jmse) {
			jmse.printStackTrace(System.err);
		}
	}

}
