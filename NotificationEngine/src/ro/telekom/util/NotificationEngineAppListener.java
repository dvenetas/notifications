package ro.telekom.util;

import ro.telekom.exception.MissingPropertyFileException;
import ro.telekom.log.CustomLogger;
import ro.telekom.value.GenericProperties;
import ro.telekom.value.SQLProperties;
import weblogic.application.ApplicationLifecycleListener;
import weblogic.application.ApplicationLifecycleEvent;

public class NotificationEngineAppListener extends ApplicationLifecycleListener {
	public void postStart(ApplicationLifecycleEvent evt) {
		CustomLogger.getInstance();
		try {
			GenericProperties.refresh();
			SQLProperties.refresh();
		} catch (MissingPropertyFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} // postStart

	public void preStop(ApplicationLifecycleEvent evt) {
		CustomLogger.refresh();
	} // preStop
}
