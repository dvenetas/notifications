package ro.telekom.util;

import java.util.Hashtable;
import java.util.Properties;

import javax.naming.*;
import javax.jms.*;

import ro.telekom.exception.MissingPropertyFileException;
import ro.telekom.value.GenericProperties;

public class JMSMessageHandler {
	private static JMSMessageHandler jmsMessageHandler;
	private InitialContext ctx = null;
	private QueueConnectionFactory qcf = null;
	private QueueConnection qc = null;
	private QueueSession qsess = null;
	private Queue q = null;
	private QueueSender qsndr = null;
	private TextMessage message = null;

	private JMSMessageHandler() throws MissingPropertyFileException{
		Properties p = GenericProperties.getInstance();

		Hashtable<String,String> properties = new Hashtable<String, String>();
		properties.put(Context.INITIAL_CONTEXT_FACTORY,
				"weblogic.jndi.WLInitialContextFactory");
		properties.put(Context.PROVIDER_URL, p.getProperty("URL"));
		properties.put(Context.SECURITY_PRINCIPAL, p.getProperty("USER"));
		properties.put(Context.SECURITY_CREDENTIALS, p.getProperty("PASSWORD"));
		try{
			ctx = new InitialContext(properties);
		}catch(NamingException e){
			e.printStackTrace();
			throw new MissingPropertyFileException(e.getMessage(), e.getCause());
		}
	}

	public static synchronized JMSMessageHandler init() throws MissingPropertyFileException{
		if(jmsMessageHandler == null){
			jmsMessageHandler = new JMSMessageHandler();
		}
		return jmsMessageHandler;
	}

	public void sendTextMessage(String messageText, String queue, String cf) {
		// create InitialContext
		try {
			
			qcf = (QueueConnectionFactory) ctx.lookup(cf);
			qc = qcf.createQueueConnection();
			qsess = qc.createQueueSession(false, 0);
			q = (Queue) ctx.lookup(queue);
			qsndr = qsess.createSender(q);
			message = qsess.createTextMessage();
			message.setText(messageText);
			qsndr.send(message);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		
	}
	
	public void close() throws MissingPropertyFileException{
		try{
		message = null;
		qsndr.close();
		qsndr = null;
		q = null;
		qsess.close();
		qsess = null;
		qc.close();
		qc = null;
		qcf = null;
		ctx = null;
		}catch(Exception e){
			e.printStackTrace();
			throw new MissingPropertyFileException(e.getMessage(), e.getCause());
		}
		
	}

}
